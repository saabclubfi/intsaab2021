# intsaab2021

International Saab Club meeting 2021

## Backend

See https://gitlab.com/saabclubfi/intsaab2021/-/blob/master/backend/README.md

API spec available at http://saabclubfi.gitlab.io/intsaab2021/

## Frontend

See https://gitlab.com/saabclubfi/intsaab2021/-/blob/master/gatsby-site/README.md
