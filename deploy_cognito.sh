
if [ -z "${1:-}" ] || [ $1 != "dev" -a $1 != "staging" -a $1 != "prod" ]
then
    echo "Usage: deploy_cognito <dev|staging|prod>"
    exit -1
fi

AWSCLI_EXTRA_ARGS=""
if [ $1 == "prod" ]
then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

set -eux

AWS_DEFAULT_REGION=eu-west-1 \
aws cloudformation deploy ${AWSCLI_EXTRA_ARGS} \
    --template-file "infrastructure/cognito.template.yml" \
    --stack-name "IntSaab2021-$1-Cognito" \
    --parameter-overrides "DeploymentEnvironment=$1"\
    --no-fail-on-empty-changeset \
    --capabilities CAPABILITY_IAM
