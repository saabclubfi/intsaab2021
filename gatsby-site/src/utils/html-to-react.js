import React from 'react';
import HtmlToReact from 'html-to-react';
import Img from 'gatsby-image';
import ReactPlayer from 'react-player';

const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);
const htmlToReactParser = new HtmlToReact.Parser();

export const reactElement = ({ files, processedText }) => {
  const processingInstructions = [
    {
      shouldProcessNode: (node) => node.type === 'tag' && node.name === 'img',
      processNode: (node, children, index) => {
        const drupalId = node.attribs['data-entity-uuid'];
        for (let edge of files.edges) {
          if (edge.node.drupal_id === drupalId) {
            return <Img fluid={edge.node.localFile.childImageSharp.fluid} />;
          }
        }
        return null;
      },
    },
    {
      shouldProcessNode: (node) =>
        node.type === 'tag' &&
        node.name === 'p' &&
        node.children &&
        node.children[0] &&
        node.children[0].attribs &&
        node.children[0].attribs.href &&
        node.children[0].attribs.href.includes('https://vimeo.com'),
      processNode: (node) => {
        return (
          <div
            style={{
              display: 'flex',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: '1rem',
              marginBottom: '1rem',
            }}
          >
            <ReactPlayer url={node.children[0].attribs.href} controls={true} />
          </div>
        );
      },
    },
    {
      // Anything else
      shouldProcessNode: () => true,
      processNode: processNodeDefinitions.processDefaultNode,
    },
  ];

  const isValidNode = () => true;

  return htmlToReactParser.parseWithInstructions(
    processedText,
    isValidNode,
    processingInstructions,
  );
};
