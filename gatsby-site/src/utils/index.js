const calculateReadTime = (text) => {
  return Math.round(text.split(' ').length / 265);
};

const formatTimestamp = (timestamp) => {
  const date = Number.isNaN(Date.parse(timestamp))
    ? new Date()
    : new Date(timestamp);
  const month = date.toLocaleString('en-us', { month: 'long' });
  return `${month} ${date.getDate()}, ${date.getFullYear()}`;
};

const slugify = (title) =>
  title
    .toLowerCase()
    .replace(/å/g, 'a')
    .replace(/ä/g, 'a')
    .replace(/ö/g, 'o')
    .replace(/ /g, '-')
    .replace(/:/g, '');

module.exports = {
  calculateReadTime,
  formatTimestamp,
  slugify,
};
