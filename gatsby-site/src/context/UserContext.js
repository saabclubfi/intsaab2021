import React from 'react';

import Auth from '../services/auth';
import { getUserProfile, getCart } from '../services/api';

let UserContext;
const { Provider } = (UserContext = React.createContext());

const UserProvider = (props) => {
  const [user, setUser] = React.useState(null);
  const [cart, setCart] = React.useState(null);

  const updateCart = React.useCallback(async () => {
    const cart = await getCart().catch(() => ({}));
    setCart(cart);
  }, []);

  React.useEffect(() => {
    updateCart();
  }, [updateCart]);

  const login = async ({ email, password }) => {
    const result = await Auth.SignIn({ email, password });

    if (result.error) {
      setUser(null);
      return result;
    }

    const profile = await getUserProfile().catch(() => ({}));
    setUser({ ...profile, ...result.user });
    updateCart();

    return result;
  };

  const logout = async () => {
    const result = await Auth.signOut();
    if (result) {
      setUser(null);
      setCart(null);
    }
  };

  const tryToLoadUser = React.useCallback(async () => {
    try {
      const {
        idToken: {
          payload: { email },
        },
      } = await Auth.getTokens();
      const profile = await getUserProfile().catch(() => ({}));
      setUser({ ...profile, email });
    } catch {
      setUser(null);
    }
  }, []);

  return (
    <Provider
      value={{
        user,
        cart,
        login,
        logout,
        signUp: Auth.signUp,
        resendSignUp: Auth.resendSignUp,
        confirmSignUp: Auth.confirmSignUp,
        tryToLoadUser,
        forgotPassword: Auth.forgotPassword,
        forgotPasswordSubmit: Auth.forgotPasswordSubmit,
        updateCart,
      }}
    >
      {props.children}
    </Provider>
  );
};

export { UserProvider, UserContext };
