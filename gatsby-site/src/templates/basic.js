import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';

import Layout from '../components/layout';
import Metatags from '../components/metatags';
import { reactElement } from '../utils/html-to-react';

import styles from './basic.module.scss';

const Basic = ({ data }) => {
  const post = data.nodeIntsaab2021Basicpage;
  const files = data.allFileFile;

  const { title, body, field_slug: slug, relationships } = post;

  return (
    <Layout>
      <Metatags
        title={title}
        image={
          relationships.field_paakuva &&
          relationships.field_paakuva.localFile &&
          relationships.field_paakuva.localFile.childImageSharp &&
          relationships.field_paakuva.localFile.childImageSharp.fixed &&
          relationships.field_paakuva.localFile.childImageSharp.fixed.src
        }
        type="article"
        url={process.env.SITE_URL ? `${process.env.SITE_URL}/${slug}` : ''}
      />
      <div className={styles.articleContainer}>
        {relationships &&
          relationships.field_paakuva &&
          relationships.field_paakuva.localFile &&
          relationships.field_paakuva.localFile.childImageSharp &&
          relationships.field_paakuva.localFile.childImageSharp.fluid && (
            <div className={styles.imageContainer}>
              <Img
                fluid={
                  relationships.field_paakuva.localFile.childImageSharp.fluid
                }
              />
            </div>
          )}
        <div className={styles.textContainer}>
          <h3 className={styles.title}>{title}</h3>
          {reactElement({ files, processedText: body.processed })}
        </div>
      </div>
    </Layout>
  );
};

export const query = graphql`
  query($id: String!) {
    nodeIntsaab2021Basicpage(id: { eq: $id }) {
      title
      body {
        processed
      }
      created
      field_slug
      relationships {
        field_paakuva {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
              fixed(width: 1200) {
                ...GatsbyImageSharpFixed
              }
            }
          }
        }
      }
    }
    allFileFile {
      edges {
        node {
          drupal_id
          localFile {
            childImageSharp {
              fluid(maxWidth: 1800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`;

export default Basic;
