import React from 'react';
import { Link, graphql } from 'gatsby';
import { FaArrowLeft } from 'react-icons/fa';
import Img from 'gatsby-image';

import Layout from '../components/layout';
import Metatags from '../components/metatags';
import { calculateReadTime, formatTimestamp } from '../utils';
import { reactElement } from '../utils/html-to-react';

import styles from './article.module.scss';

const Article = ({ data, pageContext }) => {
  const post = data.nodeIntsaabblogarticle;
  const files = data.allFileFile;

  const {
    title,
    body,
    field_julkaisupaiva: timestamp,
    field_kirjoittaja: author,
    field_tiivistelma: description,
    field_kuvakrediitti: imageCredit,
    relationships,
  } = post;

  return (
    <Layout>
      <Metatags
        title={title}
        image={relationships.field_paakuva.localFile.childImageSharp.fixed.src}
        type="article"
        url={
          process.env.SITE_URL
            ? `${process.env.SITE_URL}/blog/${pageContext.slug}`
            : ''
        }
        description={description}
      />
      <Link to="/blog" className={styles.link}>
        <h2 className={styles.header}>IntSaab2021 Blog</h2>
      </Link>
      <div className={styles.articleContainer}>
        <div className={styles.imageContainer}>
          <Img
            fluid={relationships.field_paakuva.localFile.childImageSharp.fluid}
          />
        </div>
        <div className={styles.textContainer}>
          <h3 className={styles.title}>{title}</h3>
          <h5 className={styles.articleInfo}>
            {author} wrote on {formatTimestamp(timestamp)} --{' '}
            {calculateReadTime(body.processed)}min read
          </h5>
          {imageCredit && (
            <h5 className={styles.imageCredit}>Images by: {imageCredit}</h5>
          )}
          {reactElement({ files, processedText: body.processed })}
          <Link to="/blog" className={styles.link}>
            <div className={styles.backToBlogContainer}>
              <FaArrowLeft />
              <p className={styles.back}>Back</p>
            </div>
          </Link>
        </div>
      </div>
    </Layout>
  );
};

export const query = graphql`
  query($id: String!) {
    nodeIntsaabblogarticle(id: { eq: $id }) {
      title
      body {
        processed
      }
      created
      field_julkaisupaiva
      field_kirjoittaja
      field_tiivistelma
      field_kuvakrediitti
      relationships {
        field_paakuva {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
              fixed(width: 1200) {
                ...GatsbyImageSharpFixed
              }
            }
          }
        }
      }
    }
    allFileFile {
      edges {
        node {
          drupal_id
          localFile {
            childImageSharp {
              fluid(maxWidth: 1800, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`;

export default Article;
