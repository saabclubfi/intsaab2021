import Axios from 'axios';

import auth from '../auth/index';

const API_URL = process.env.GATSBY_API_URL;

const buildHeaders = async () => {
  const tokens = await auth.getTokens();

  return {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${tokens?.accessToken?.jwtToken}`,
  };
};

export const getUserProfile = async () => {
  const response = await Axios.get(`${API_URL}/user`, {
    headers: await buildHeaders(),
  });

  if (response.data.error) {
    throw new Error(response.data.error);
  }

  return {
    ...response.data.data,
    subscriptions: formatSubscriptions(response?.data?.data?.subscriptions),
  };
};

export const getSubscriptions = async () => {
  return ['new_blog_posts', 'other_news'];
};

export const saveUserProfile = async (data) => {
  const response = await Axios.post(`${API_URL}/user`, data, {
    headers: await buildHeaders(),
  });

  if (response.data.error) {
    throw new Error(response.data.error);
  }

  return response.data;
};

export const getParticipants = async () => {
  const response = await Axios.get(`${API_URL}/user/participant`, {
    headers: await buildHeaders(),
  });

  if (response.data.error) {
    throw new Error(response.data.error);
  }

  return response.data;
};

export const saveParticipant = async (participant) => {
  const headers = await buildHeaders();
  const response = await (participant.id
    ? Axios.put(`${API_URL}/user/participant/${participant.id}`, participant, {
        headers,
      })
    : Axios.post(`${API_URL}/user/participant`, participant, { headers }));

  if (response.data.error) {
    throw new Error(response.data.error);
  }
};

export const deleteParticipant = async (id) => {
  const response = await Axios.delete(`${API_URL}/user/participant/${id}`, {
    headers: await buildHeaders(),
  });

  if (response.data.error) {
    throw new Error(response.data.error);
  }
};

export const getCart = async () => {
  const response = await Axios.get(`${API_URL}/cart`, {
    headers: await buildHeaders(),
  });

  if (response.data.error) {
    throw new Error(response.data.error);
  }

  return response.data.data;
};

const formatSubscriptions = (subscriptions) => {
  return subscriptions.reduce(
    (result, subscription) => ({
      ...result,
      [subscription]: subscription,
    }),
    {},
  );
};
