const tCognitoError = ({ code = '' }) => {
  switch (code) {
    case 'UsernameExistsException':
      return 'The email address has already been registered.';

    case 'CodeMismatchException':
      return 'The inserted code is incorrect. Please check your email from info@saabclub.fi and paste the given code here.';

    case 'NotAuthorizedException':
      return 'Username or password is incorrect.';

    case 'LimitExceededException':
      return 'Attempt limit exceeded, please try after some time.';

    case 'ExpiredCodeException':
      return 'Invalid code provided, please request a code again.';

    case 'UserNotConfirmedException':
      return 'User is not confirmed. Please contact the admins.';

    default:
      console.log(code);
      return 'Oh snap! Please contact the admins.';
  }
};

export default tCognitoError;
