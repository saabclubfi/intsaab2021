import { Auth } from 'aws-amplify';

import tCognitoError from './utils';

export const signUp = async ({ email, password }) => {
  try {
    const { user } = await Auth.signUp({
      username: email,
      password,
      attributes: {
        email,
      },
      validationData: [],
    });

    return {
      user,
    };
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const confirmSignUp = async ({ email, code }) => {
  try {
    const data = await Auth.confirmSignUp(email, code, {
      forceAliasCreation: true,
    });

    return {
      data,
    };
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const resendSignUp = async ({ email }) => {
  try {
    await Auth.resendSignUp(email);
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const signOut = async () => {
  try {
    await Auth.signOut();
    return true;
  } catch (error) {
    console.error(tCognitoError(error));
    return false;
  }
};

export const SignIn = async ({ email, password }) => {
  try {
    const result = await Auth.signIn(email, password);
    return { user: { email: result?.attributes?.email }, error: null };
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const getTokens = async () => {
  try {
    const session = await Auth.currentSession();
    return session;
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const forgotPassword = async ({ email }) => {
  try {
    await Auth.forgotPassword(email);
    return { error: null };
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};

export const forgotPasswordSubmit = async ({ email, code, password }) => {
  try {
    await Auth.forgotPasswordSubmit(email, code, password);
    return { error: null };
  } catch (error) {
    return {
      error: tCognitoError(error),
    };
  }
};
