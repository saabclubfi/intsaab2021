import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';
import Img from 'gatsby-image';

import { formatTimestamp, calculateReadTime, slugify } from '../utils';

import Layout from '../components/layout';

const BlogEntry = (article) => {
  const {
    body,
    field_julkaisupaiva: timestamp,
    field_kirjoittaja: author,
    index,
    relationships,
    title,
  } = article;

  const Text = () => (
    <div style={{ width: '50%' }}>
      <h3>{title}</h3>
      <h5 style={{ color: '#699' }}>
        {author} wrote on {formatTimestamp(timestamp)} --{' '}
        {calculateReadTime(body.processed)}min read
      </h5>
      <p>
        {body.processed.replace(/<[^>]*>/gi, '').substr(0, 250 - 3) + '...'}
      </p>
    </div>
  );

  const Image = () => (
    <div style={{ width: '45%' }}>
      <Img
        fluid={relationships.field_paakuva.localFile.childImageSharp.fluid}
      />
    </div>
  );

  return (
    <Link to={`/blog/${slugify(title)}`} style={{ textDecoration: 'none' }}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: '5vw',
          cursor: 'pointer',
          color: '#014455',
        }}
      >
        {index % 2 === 0 ? (
          <React.Fragment>
            <Text />
            <Image />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Image />
            <Text />
          </React.Fragment>
        )}
      </div>
    </Link>
  );
};

const Blog = () => {
  const data = useStaticQuery(graphql`
    {
      allNodeIntsaabblogarticle(sort: { fields: created, order: ASC }) {
        edges {
          node {
            id
            title
            body {
              processed
            }
            field_kirjoittaja
            field_julkaisupaiva
            relationships {
              field_paakuva {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 800, quality: 80) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `);

  return (
    <Layout>
      <h2
        style={{
          color: '#014455',
          fontSize: '3rem',
          textAlign: 'center',
          marginTop: '2rem',
          marginBottom: '5vw',
        }}
      >
        IntSaab2021 Blog
      </h2>
      {data.allNodeIntsaabblogarticle.edges &&
        data.allNodeIntsaabblogarticle.edges.map((blog, index) => (
          <BlogEntry key={index} {...blog.node} index={index} />
        ))}
    </Layout>
  );
};

export default Blog;
