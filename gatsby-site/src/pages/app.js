import React, { useContext, useEffect } from 'react';
import { Router } from '@reach/router';

import Layout from '../components/layout';
import Profile from '../components/profile/profile';
import PrivateRoute from '../components/private-route';
import { UserContext } from '../context/UserContext';

const App = () => {
  const { tryToLoadUser } = useContext(UserContext);

  useEffect(() => {
    tryToLoadUser();
  }, [tryToLoadUser]);

  return (
    <Layout>
      <Router>
        <PrivateRoute path="/app/profile" component={Profile} />
      </Router>
    </Layout>
  );
};

export default App;
