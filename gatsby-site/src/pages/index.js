import React, { useContext, useEffect } from 'react';

import InfoBlock from '../components/infoblock';
import Layout from '../components/layout';
import { UserContext } from '../context/UserContext';

const IndexPage = () => {
  const { tryToLoadUser } = useContext(UserContext);

  useEffect(() => {
    tryToLoadUser();
  }, [tryToLoadUser]);

  return (
    <Layout>
      <InfoBlock />
    </Layout>
  );
};

export default IndexPage;
