import React from 'react';

export default ({ mailto }) => {
  const randomGen = (min, max) => {
    const seen = {};
    return () => {
      let rnd;
      while (true) {
        rnd = Math.floor(Math.random() * (max - min + 1));
        if (!seen[rnd]) {
          seen[rnd] = true;
          break;
        }
      }
      return rnd;
    };
  };

  const getNextRandomInt = randomGen(100, 1000);

  const randomMail =
    // turn the mailto string into an array
    mailto
      .split('')
      // assign each element a col position based on the original position (1 to x)
      .map((c, i) => ({ c, pos: i + 1 }))
      // assign each element of the array a random unique integer between m and n
      .map((o) => ({ ...o, shuffledPos: getNextRandomInt() }))
      // order the array based on the integer
      .sort((a, b) => (a.shuffledPos > b.shuffledPos ? 1 : -1));

  const createMarkup = () => ({
    __html: randomMail
      .map(({ pos, c }) => `<span style="order: ${pos};">${c}</span>`)
      .join(''),
  });

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      dangerouslySetInnerHTML={createMarkup()}
    />
  );
};
