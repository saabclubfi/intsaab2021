import * as React from 'react';

import styles from './input.module.scss';

const Input = ({
  type,
  name,
  value,
  onChange,
  placeholder,
  icon: Icon,
  ...rest
}) => (
  <div className={styles.inputContainer}>
    <input
      type={type}
      name={name}
      value={value}
      onChange={onChange}
      className={styles.input}
      placeholder={placeholder}
      {...rest}
    />
    <div className={styles.iconContainer}>
      <Icon className={styles.icon} />
    </div>
  </div>
);

export default Input;
