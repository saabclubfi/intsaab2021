import React from 'react';

import SocialMedia from './social-media';
import ObfuscatedEmail from './obfuscated-email';

import styles from './footer.module.scss';

const Footer = () => (
  <>
    <hr className={styles.separator} />
    <SocialMedia />
    <div className={styles.contactEmailContainer}>
      <ObfuscatedEmail mailto="info@intsaab2021.com" />
      <p className={styles.date}>13–15 August 2021</p>
    </div>
  </>
);

export default Footer;
