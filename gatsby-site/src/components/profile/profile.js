import React from 'react';

import User from './user';
import Participants from './participants';
import { PAGES } from './shared';

const useMessage = (initialValue = '') => {
  const [message, setMessage] = React.useState(initialValue);

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      if (message) {
        setMessage('');
      }
    }, 2000);

    return () => clearTimeout(timeout);
  }, [message, setMessage]);

  return [message, setMessage];
};

const Profile = () => {
  const [page, setPage] = React.useState(PAGES.USER);
  const [message, setMessage] = useMessage('');

  const commonProps = {
    page,
    setPage,
    message,
    setMessage,
  };

  switch (page) {
    case PAGES.USER:
      return <User {...commonProps} />;
    case PAGES.PARTICIPANTS:
      return <Participants {...commonProps} />;
    default:
      return null;
  }
};

export default Profile;
