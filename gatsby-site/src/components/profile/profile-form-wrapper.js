import React from 'react';

import { PAGES } from './shared';

import styles from './profile.module.scss';

const pageToTitle = {
  [PAGES.USER]: <span>Profile</span>,
  [PAGES.PARTICIPANTS]: <span>Participants</span>,
  [PAGES.LOADING]: <span>Loading...</span>,
};

const Title = ({ page, setPage }) => {
  const changePage = () => {
    page === PAGES.USER ? setPage(PAGES.PARTICIPANTS) : setPage(PAGES.USER);
  };

  return (
    <div
      className={styles.tabContainer}
      onClick={changePage}
      role="button"
      tabIndex={0}
      onKeyDown={changePage}
    >
      <div
        className={
          page === PAGES.USER ? `${styles.active} ${styles.tab}` : styles.tab
        }
      >
        {pageToTitle[PAGES.USER]}
      </div>
      <div
        className={
          page === PAGES.PARTICIPANTS
            ? `${styles.active} ${styles.tab}`
            : styles.tab
        }
      >
        {pageToTitle[PAGES.PARTICIPANTS]}
      </div>
      {page === PAGES.LOADING && (
        <span className={styles.loadingBanner}>{pageToTitle[page]}</span>
      )}
    </div>
  );
};

const ProfileFormWrapper = ({ page, setPage, message, children }) => {
  const [edit, setEdit] = React.useState(false);

  return (
    <div className={styles.profilePage}>
      <Title page={page} setPage={setPage} />
      <p>{message}</p>
      <div className={styles.formContainer}>{children({ edit, setEdit })}</div>
    </div>
  );
};

export default ProfileFormWrapper;
