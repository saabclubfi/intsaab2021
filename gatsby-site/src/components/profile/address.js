import React from 'react';
import { FaAddressCard, FaFlag } from 'react-icons/fa';
import { countries } from 'countries-list';

import Input from '../input';
import Select from '../select';

const COUNTRIES = Object.entries(countries).reduce(
  (result, [abbr, { name }]) => ({ ...result, [name]: abbr }),
  {},
);

const Address = ({
  onAddressChange,
  address: { line1, line2, city, zip, country },
  readOnly,
}) => {
  return (
    <>
      <p>Address</p>
      <div>
        <Input
          type="text"
          name="line1"
          value={line1}
          onChange={onAddressChange}
          placeholder="Line 1"
          icon={FaAddressCard}
          readOnly={readOnly}
        />
        <Input
          type="text"
          name="line2"
          value={line2}
          onChange={onAddressChange}
          placeholder="Line 2"
          icon={FaAddressCard}
          readOnly={readOnly}
        />
        <Input
          type="text"
          name="city"
          value={city}
          onChange={onAddressChange}
          placeholder="City"
          icon={FaAddressCard}
          readOnly={readOnly}
        />
        <Input
          type="text"
          name="zip"
          value={zip}
          onChange={onAddressChange}
          placeholder="Zip"
          icon={FaAddressCard}
          readOnly={readOnly}
        />
        <Select
          name="country"
          value={country}
          onChange={onAddressChange}
          placeholder="Country"
          icon={FaFlag}
        >
          <option value="">Choose a country</option>
          {Object.entries(COUNTRIES)
            .sort(([name1], [name2]) => name1 > name2)
            .map(([name, abbr]) => (
              <option value={abbr} key={abbr}>
                {name}
              </option>
            ))}
        </Select>
      </div>
    </>
  );
};

export default Address;
