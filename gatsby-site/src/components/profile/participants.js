import React from 'react';
import { FaUserCircle, FaEdit, FaTrashAlt } from 'react-icons/fa';

import Input from '../input';
import {
  deleteParticipant,
  getParticipants,
  saveParticipant,
} from '../../services/api';
import { PAGES, address } from './shared';
import ProfileFormWrapper from './profile-form-wrapper';
import Address from './address';
import { UserContext } from '../../context/UserContext';

import styles from './profile.module.scss';

const initialParticipant = {
  name: '',
  age: '',
  address: {
    ...address,
  },
};

const Participants = ({ page, setPage, message, setMessage }) => {
  const [participants, setParticipants] = React.useState(null);
  const [participant, setParticipant] = React.useState(initialParticipant);
  const [isAdding, setIsAdding] = React.useState(false);
  const { updateCart } = React.useContext(UserContext);

  React.useEffect(() => {
    updateCart();
  }, [participants, updateCart]);

  const fetchParticipants = React.useCallback(async () => {
    try {
      const result = await getParticipants();
      setParticipants(result.data);
    } catch (error) {
      setMessage(error.message);
      setParticipants([]);
    }
  }, [setParticipants, setMessage]);

  React.useEffect(() => {
    fetchParticipants();
  }, [fetchParticipants]);

  const onChange = (event) => {
    setParticipant({
      ...participant,
      [event.target.name]: event.target.value,
    });
  };

  const onAgeChange = (event) => {
    const regex = /^\d{0,2}$/;
    regex.test(event.target.value) && onChange(event);
  };

  const onAddressChange = (event) => {
    setParticipant({
      ...participant,
      address: {
        ...participant.address,
        [event.target.name]: event.target.value,
      },
    });
  };

  const clearAndHideParticipant = () => {
    setIsAdding(false);
    setParticipant(initialParticipant);
  };

  const onSubmit = async (event) => {
    event.preventDefault();

    setMessage('Saving participant...');

    // TODO: validate participant a bit

    try {
      await saveParticipant(participant);
      fetchParticipants();
    } catch (error) {
      setMessage(error.message);
    } finally {
      clearAndHideParticipant();
    }
  };

  const removeParticipant = async (id) => {
    try {
      await deleteParticipant(id);
      fetchParticipants();
    } catch (error) {
      setMessage(error.message);
    } finally {
      clearAndHideParticipant();
    }
  };

  const editParticipant = (id) => {
    setParticipant(participants.find((p) => p.id === id));
    setIsAdding(true);
  };

  if (!participants) {
    return (
      <ProfileFormWrapper page={PAGES.LOADING} message={message}>
        {() => null}
      </ProfileFormWrapper>
    );
  }

  return (
    <>
      <ProfileFormWrapper page={page} setPage={setPage} message={message}>
        {() => (
          <>
            <p className={styles.infoText}>
              Enter here all participants traveling with you, including
              children. Age is a mandatory field because the discount for kids
              depends on their age. For adult participants it's ok to enter 18
              if you prefer not to reveal the actual figure. Leave address empty
              if it's the same as your own.
            </p>
            {participants.length === 0 && <span>No participants yet.</span>}
            {participants.map((participant) => (
              <div
                className={styles.participantCardContainer}
                key={participant.name}
              >
                <span>{participant.name}</span>
                <span>
                  <FaEdit
                    className={styles.control}
                    onClick={() => editParticipant(participant.id)}
                  />
                  <FaTrashAlt
                    className={styles.control}
                    onClick={() => removeParticipant(participant.id)}
                  />
                </span>
              </div>
            ))}
            <hr className="divider"></hr>
            {isAdding && (
              <form onSubmit={onSubmit}>
                <fieldset>
                  <label htmlFor="name">Name</label>
                  <Input
                    id="name"
                    type="text"
                    name="name"
                    value={participant.name}
                    onChange={onChange}
                    placeholder="Name"
                    icon={FaUserCircle}
                    required
                  />
                  <label htmlFor="age">Age</label>
                  <Input
                    id="age"
                    type="text"
                    name="age"
                    value={participant.age}
                    onChange={onAgeChange}
                    placeholder="Age"
                    icon={FaUserCircle}
                    required
                  />
                  <Address
                    onAddressChange={onAddressChange}
                    address={participant.address}
                  />
                  <div className={styles.submitButtonContainer}>
                    <button className={styles.button}>SAVE</button>
                  </div>
                </fieldset>
              </form>
            )}
            {isAdding ? (
              <div className={styles.hideButtonContainer}>
                <span
                  className={styles.button}
                  role="button"
                  onKeyDown={() => setIsAdding(!isAdding)}
                  tabIndex={0}
                  onClick={() => setIsAdding(!isAdding)}
                >
                  Hide
                </span>
              </div>
            ) : (
              <div className={styles.submitButtonContainer}>
                <button
                  className={styles.button}
                  onClick={() => setIsAdding(!isAdding)}
                >
                  Add participant
                </button>
              </div>
            )}
          </>
        )}
      </ProfileFormWrapper>
    </>
  );
};

export default Participants;
