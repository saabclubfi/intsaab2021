import React from 'react';
import { FaUserCircle, FaPhone, FaEdit, FaCar } from 'react-icons/fa';

import Input from '../input';
import {
  getSubscriptions,
  getUserProfile,
  saveUserProfile,
} from '../../services/api';
import Address from './address';
import ProfileFormWrapper from './profile-form-wrapper';
import { PAGES, address } from './shared';
import { UserContext } from '../../context/UserContext';

import styles from './profile.module.scss';

const initialUser = {
  name: '',
  phone: '',
  address,
  subscriptions: [],
};

const translateSubscription = (key, lang = 'en') => {
  return {
    en: {
      new_blog_posts: 'New Blog Posts',
      other_news: 'Other News',
    },
  }[lang][key];
};

const Subscriptions = ({ subscriptions, onSubscriptionsChange, state }) => {
  return (
    <>
      <p>Subscriptions</p>
      <div>
        {subscriptions.map((sub) => (
          <div className={styles.subscriptionContainer} key={sub}>
            <label htmlFor={sub}>{translateSubscription(sub)}</label>
            <Input
              id={sub}
              type="checkbox"
              name={sub}
              checked={!!state[sub]}
              onChange={onSubscriptionsChange}
              icon={() => null}
            />
          </div>
        ))}
      </div>
    </>
  );
};

const User = ({ page, setPage, message, setMessage }) => {
  const [user, setUser] = React.useState(null);
  const [subscriptions, setSubscriptions] = React.useState([]);
  const { updateCart } = React.useContext(UserContext);

  React.useEffect(() => {
    (async () => {
      try {
        const result = await getUserProfile();
        setUser(result);
      } catch (error) {
        setMessage(error.message);
        setUser(initialUser);
      }
    })();
  }, [setUser, setMessage]);

  React.useEffect(() => {
    (async () => {
      const result = await getSubscriptions();
      setSubscriptions(result);
    })();
  }, [setSubscriptions]);

  React.useEffect(() => {
    updateCart();
  }, [user, updateCart]);

  const onChange = (event) => {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  };

  const onAddressChange = (event) => {
    setUser({
      ...user,
      address: {
        ...user.address,
        [event.target.name]: event.target.value,
      },
    });
  };

  const onSubscriptionsChange = (event) => {
    if (user.subscriptions[event.target.name]) {
      const { [event.target.name]: removed, ...rest } = user.subscriptions;

      return setUser({
        ...user,
        subscriptions: {
          ...rest,
        },
      });
    }

    setUser({
      ...user,
      subscriptions: {
        ...user.subscriptions,
        [event.target.name]: event.target.name,
      },
    });
  };

  const onSubmit = async (event) => {
    event.preventDefault();

    setMessage('Saving user...');

    try {
      await saveUserProfile({
        ...user,
        subscriptions: Object.keys(user.subscriptions),
      });
      setMessage('User saved!');
    } catch (error) {
      setMessage(error.message);
    }
  };

  if (!user) {
    return (
      <ProfileFormWrapper page={PAGES.LOADING} message={message}>
        {() => null}
      </ProfileFormWrapper>
    );
  }

  return (
    <ProfileFormWrapper page={page} setPage={setPage} message={message}>
      {({ edit, setEdit }) => (
        <>
          <p className={styles.infoText}>
            This is where you enter the details of yourself and your Saab. Save
            your profile first and then add everyone traveling with you as
            participants.
          </p>
          <form onSubmit={onSubmit}>
            <div className={styles.editContainer}>
              <label htmlFor="name">Name</label>
              <div
                className={styles.editButton}
                onClick={() => setEdit(!edit)}
                onKeyDown={() => setEdit(!edit)}
                role="button"
                tabIndex="0"
              >
                Edit
                <FaEdit />
              </div>
            </div>
            <fieldset disabled={!edit}>
              <Input
                id="name"
                type="text"
                name="name"
                value={user.name}
                onChange={onChange}
                placeholder="Name"
                icon={FaUserCircle}
              />
              <label htmlFor="phone">Phone</label>
              <Input
                id="phone"
                type="tel"
                name="phone"
                value={user.phone}
                onChange={onChange}
                placeholder="Phone"
                icon={FaPhone}
              />
              <label htmlFor="car">Saab</label>
              <Input
                id="car"
                type="text"
                name="car"
                value={user.car}
                onChange={onChange}
                placeholder="Enter your Saab model"
                icon={FaCar}
              />
              <Address
                onAddressChange={onAddressChange}
                address={user.address}
              />
              <Subscriptions
                onSubscriptionsChange={onSubscriptionsChange}
                subscriptions={subscriptions}
                state={user.subscriptions}
              />
              <div className={styles.submitButtonContainer}>
                <button className={styles.button}>SAVE</button>
              </div>
            </fieldset>
          </form>
        </>
      )}
    </ProfileFormWrapper>
  );
};

export default User;
