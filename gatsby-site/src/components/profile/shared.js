export const PAGES = {
  PARTICIPANTS: 'PARTICIPANTS',
  USER: 'USER',
  LOADING: 'LOADING',
};

export const address = {
  line1: '',
  city: '',
  zip: '',
  country: '',
};
