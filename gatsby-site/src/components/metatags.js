import React from 'react';
import Helmet from 'react-helmet';

const TITLE_SUFFIX = 'IntSaab2021';

export default ({
  title,
  image,
  type = 'website',
  url,
  description,
  keywords,
}) => (
  <Helmet
    meta={[
      {
        name: 'description',
        content: description,
      },
      { name: 'keywords', content: keywords },
    ]}
  >
    {title && <title>{`${title} | ${TITLE_SUFFIX}`}</title>}
    <meta property="og:image" content={image} />
    <meta property="og:type" content={type} />
    {url && <meta property="og:url" content={url} />}
    {description && <meta property="og:description" content={description} />}
    {description && <meta name="description" content={description} />}
    <meta name="twitter:card" content="summary_large_image" />
    <html lang="en" />
  </Helmet>
);
