import React from 'react';
import { render } from '@testing-library/react';

import ProfileFormWrapper from '../../profile/profile-form-wrapper';

describe('ProfileFormWrapper', () => {
  it('renders loading title', () => {
    const { getByText } = render(
      <ProfileFormWrapper page="LOADING" setPage={() => undefined} message="">
        {() => <div>test children</div>}
      </ProfileFormWrapper>,
    );

    const title = getByText('Loading...');

    expect(title).toBeInTheDocument();
  });

  it('renders profile title', () => {
    const { getByText } = render(
      <ProfileFormWrapper page="USER" setPage={() => undefined} message="">
        {() => <div>test children</div>}
      </ProfileFormWrapper>,
    );

    const title = getByText('Profile');

    expect(title).toBeInTheDocument();
  });

  it('renders participants title', () => {
    const { getByText } = render(
      <ProfileFormWrapper
        page="PARTICIPANTS"
        setPage={() => undefined}
        message=""
      >
        {() => <div>test children</div>}
      </ProfileFormWrapper>,
    );

    const title = getByText('Participants');

    expect(title).toBeInTheDocument();
  });
});
