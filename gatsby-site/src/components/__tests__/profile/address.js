import React from 'react';
import renderer from 'react-test-renderer';

import Address from '../../profile/address';

const testProps = {
  onAddressChange: () => undefined,
  address: {
    line1: 'line1',
    line2: 'line2',
    city: 'city',
    zip: 'zip',
    contry: 'country',
  },
};

describe('Address', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<Address {...testProps} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
