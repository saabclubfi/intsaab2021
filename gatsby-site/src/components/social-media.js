import React from 'react';
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';

const SocialMedia = () => {
  return (
    <div style={{ color: '#014455' }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '2rem',
          fontSize: '2rem',
        }}
      >
        <a
          href="https://facebook.com/IntSaab2021"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaFacebook
            style={{ marginRight: '15px', cursor: 'pointer', color: '#014455' }}
          />
        </a>
        <a
          href="https://instagram.com/IntSaab2021"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaInstagram
            style={{ marginRight: '15px', cursor: 'pointer', color: '#014455' }}
          />
        </a>
        <a
          href="https://twitter.com/IntSaab2021"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaTwitter
            style={{ marginRight: '0px', cursor: 'pointer', color: '#014455' }}
          />
        </a>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '.5rem',
        }}
      >
        <p style={{ textAlign: 'center', fontWeight: 'bold', margin: 0 }}>
          @IntSaab2021
        </p>
      </div>
    </div>
  );
};

export default SocialMedia;
