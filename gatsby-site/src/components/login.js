import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { FaUserCircle } from 'react-icons/fa';
import { MdMenu } from 'react-icons/md';
import styles from './login.module.scss';

const Login = ({ user, isOpen, actions, toggleIsOpen }) => (
  <div className={styles.root}>
    <div className={styles.main}>
      <FaUserCircle className={styles.icon} />
      {user ? (
        <>
          <div title={user.email} className={styles.email}>
            {user.email.slice(0, 1)}
          </div>
          <MdMenu
            className={styles.icon}
            style={{
              transform: isOpen ? 'rotate(120deg)' : 'rotate(0deg)',
            }}
            onClick={() => toggleIsOpen(!isOpen)}
          />
        </>
      ) : (
        <Link to="/login" className={styles.link}>
          Sign in
        </Link>
      )}
    </div>
    {isOpen && Boolean(user?.email) && (
      <div className={styles.actions}>
        <ul>
          {actions.map((action) => (
            <li key={action.text}>
              <div
                title={action.text}
                onClick={action.fn}
                role="button"
                tabIndex={0}
                onKeyDown={(e) => e.key === 'Enter' && action.fn()}
              >
                {action.text}
              </div>
            </li>
          ))}
        </ul>
      </div>
    )}
  </div>
);

Login.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
  }),
  isOpen: PropTypes.bool,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      fn: PropTypes.func,
    }),
  ),
  toggleIsOpen: PropTypes.func,
};

Login.defaultProps = {
  email: '',
  isOpen: false,
  actions: [],
};

export default Login;
