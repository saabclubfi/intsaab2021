import React, { useState, useContext } from 'react';
import { navigate, Link } from 'gatsby';
import { FaShoppingCart } from 'react-icons/fa';
import * as R from 'ramda';

import { UserContext } from '../context/UserContext';
import Login from './login';

import image from '../images/IntSaab2021-logo-rgb.svg';
import styles from './header.module.scss';

const formatPrice = R.compose(
  R.chain(R.flip(R.divide)(100)),
  R.of,
  R.prop('totalPrice'),
);

const Cart = ({ cart }) => {
  return (
    <div className={styles.cartContainer}>
      <FaShoppingCart />
      {formatPrice(cart)}€
    </div>
  );
};

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { user, logout, cart } = useContext(UserContext);

  const actions = [
    {
      text: 'Profile',
      fn: () => navigate('/app/profile'),
    },
    {
      text: 'Sign out',
      fn: async () => {
        await logout();
        navigate('/');
      },
    },
  ];

  return (
    <header className={styles.headerContainer}>
      <div className={styles.titleContainer}>
        <Link to="/">
          <img className={styles.logo} src={image} alt="IntSaab2021" />
        </Link>
        <div className={styles.textContainer}>
          <p className={styles.title}>IntSaab2021</p>
          <p className={styles.title}>Finland</p>
          <p className={styles.title}>13–15 August</p>
        </div>
      </div>
      {cart && cart.totalPrice > 0 && <Cart cart={cart} />}
      <div className={styles.loginContainer}>
      </div>
    </header>
  );
};

export default Header;
