import React from 'react';
import { graphql, Link, useStaticQuery } from 'gatsby';
import Img from 'gatsby-image';
import stripHtml from 'string-strip-html';

import styles from './infoblock.module.scss';

const InfoBlockItem = ({
  node: {
    field_link: link,
    relationships: { field_kuva: imageList },
    field_title: title,
    body,
  },
  index,
}) => {
  const [linkTarget, linkText] = link.split('|') || ['/', ''];

  const children = (
    <div
      className={styles.container}
      style={{
        flexDirection: index % 2 ? 'row' : 'row-reverse',
      }}
    >
      <ImagePane images={imageList} />
      <TextPane node={{ title, body, linkText }} />
    </div>
  );

  return linkTarget[0] === '/' ? (
    <Link className={styles.link} to={linkTarget}>
      {children}
    </Link>
  ) : (
    <a
      className={styles.link}
      href={linkTarget}
      target="_blank"
      rel="noopener noreferrer"
    >
      {children}
    </a>
  );
};

const ImagePane = ({ images }) => {
  const fluidImages = images.map(
    (image) =>
      image.relationships.field_media_image.localFile.childImageSharp.fluid,
  );

  return (
    <div className={styles.pane}>
      <div className={styles.imageContainer}>
        <Img fluid={fluidImages[0]} className={styles.image0} />
        {fluidImages[1] && (
          <Img fluid={fluidImages[1]} className={styles.image1} />
        )}
        {fluidImages[2] && (
          <Img fluid={fluidImages[2]} className={styles.image2} />
        )}
      </div>
    </div>
  );
};

const TextPane = ({ node: { title, body, linkText } }) => {
  return (
    <div className={styles.pane}>
      <h2 className={styles.heading2}>{title}</h2>
      <p className={styles.paragraph}>{stripHtml(body.processed)}</p>
      <div className={styles.ctaLink}>
        {linkText.length > 20 ? `${linkText.slice(0, 20)}...` : linkText}
      </div>
    </div>
  );
};

const InfoBlock = () => {
  const data = useStaticQuery(graphql`
    {
      allBlockContentIntsaabinfo {
        totalCount
        edges {
          node {
            id
            drupal_id
            body {
              value
              processed
            }
            field_link
            field_title
            relationships {
              field_kuva {
                id
                drupal_id
                name
                relationships {
                  field_media_image {
                    id
                    drupal_id
                    filename
                    localFile {
                      url
                      publicURL
                      childImageSharp {
                        fluid(maxWidth: 400) {
                          ...GatsbyImageSharpFluid
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `);

  const { edges = [] } = data.allBlockContentIntsaabinfo || {};
  return (
    <section>
      {edges.map(({ node }, index) => (
        <InfoBlockItem node={node} key={index} index={index} />
      ))}
    </section>
  );
};

export default InfoBlock;
