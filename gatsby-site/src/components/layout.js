import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';

import Header from './header';
import Footer from './footer';
import Metatags from './metatags';

import './layout.css';
import styles from './layout.module.scss';
import image from '../images/IntSaab2021-logo-rgb.svg';

const EVENT_DESCRIPTION = 'IntSaab 2021 Finland. Himos, August 13-15 2021.';

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          description
          keywords
          title
        }
      }
    }
  `);

  return (
    <>
      <Metatags
        title={data.site.siteMetadata.title}
        image={image}
        url={process.env.SITE_URL ? process.env.SITE_URL : '/'}
        description={EVENT_DESCRIPTION}
        keywords={data.site.siteMetadata.keywords}
      />
      <Header />
      <main className={styles.layout}>{children}</main>
      <Footer />
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
