import React from 'react';

import styles from './select.module.scss';

const Select = ({
  name,
  value,
  onChange,
  placeholder,
  icon: Icon,
  children,
}) => (
  <div className={styles.inputContainer}>
    <select
      name={name}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      className={styles.input}
    >
      {children}
    </select>
    <div className={styles.iconContainer}>
      <Icon className={styles.icon} />
    </div>
  </div>
);

export default Select;
