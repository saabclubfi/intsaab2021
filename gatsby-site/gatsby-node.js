/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const { slugify } = require('./src/utils');

const createArticles = (nodes, createPage) =>
  new Promise((resolve) => {
    nodes.forEach(({ node: { title, id } }) => {
      createPage({
        path: `/blog/${slugify(title)}`,
        component: require.resolve('./src/templates/article.js'),
        context: {
          id,
          slug: slugify(title),
        },
      });
    });

    return resolve();
  });

const createBasicPages = (nodes, createPage) =>
  new Promise((resolve) => {
    nodes.forEach(({ node: { id, field_slug: slug } }) => {
      createPage({
        path: `/${slug}`,
        component: require.resolve('./src/templates/basic.js'),
        context: {
          id,
        },
      });
    });

    return resolve();
  });

exports.createPages = async ({ actions: { createPage }, graphql }) => {
  const result = await graphql(`
    {
      allNodeIntsaabblogarticle {
        edges {
          node {
            id
            title
          }
        }
      }
      allNodeIntsaab2021Basicpage {
        edges {
          node {
            id
            title
            field_slug
          }
        }
      }
    }
  `);

  const articleNodes =
    (result &&
      result.data &&
      result.data.allNodeIntsaabblogarticle &&
      result.data.allNodeIntsaabblogarticle.edges) ||
    [];

  const basicPageNodes =
    (result &&
      result.data &&
      result.data.allNodeIntsaab2021Basicpage &&
      result.data.allNodeIntsaab2021Basicpage.edges) ||
    [];

  Promise.all([
    createArticles(articleNodes, createPage),
    createBasicPages(basicPageNodes, createPage),
  ]);
};
