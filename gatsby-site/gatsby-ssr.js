/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

import React from 'react';

import { UserProvider } from './src/context/UserContext';

export const wrapRootElement = ({ element }) => (
  <UserProvider>{element}</UserProvider>
);
