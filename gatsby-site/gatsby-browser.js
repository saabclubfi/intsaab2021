/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import Amplify from 'aws-amplify';
import React from 'react';

import { UserProvider } from './src/context/UserContext';

export const wrapRootElement = ({ element }) => (
  <UserProvider>{element}</UserProvider>
);

export const onClientEntry = () => {
  Amplify.configure({
    Auth: {
      region: 'eu-west-1',
      userPoolId: process.env.GATSBY_USER_POOL_ID || 'eu-west-1_iZykdXnQl',
      userPoolWebClientId:
        process.env.GATSBY_USER_POOL_CLIENT_ID || '7kh2rha8g405ibiu1vn8h8a8q4',
      mandatorySignIn: false,
    },
  });
};
