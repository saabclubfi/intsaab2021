const PRODUCTION_URL = 'https://intsaab2021.com';

const gatsbySourceFilesystem = {
  resolve: `gatsby-source-filesystem`,
  options: {
    name: `images`,
    path: `${__dirname}/src/images`,
  },
};

const gatsbyPluginManifest = {
  resolve: `gatsby-plugin-manifest`,
  options: {
    name: 'IntSaab 2021 Finland',
    short_name: 'IntSaab 2021 Finland',
    start_url: '/',
    background_color: '#2094d2',
    theme_color: '#2094d2',
    display: 'minimal-ui',
    icon: 'src/images/intsaab2021-icon.png', // This path is relative to the root of the site.
  },
};

const gatsbyPluginRobotsTxt = {
  resolve: 'gatsby-plugin-robots-txt',
  options: {
    host: process.env.SITE_URL,
    sitemap: process.env.SITE_URL + '/sitemap.xml',
    policy:
      process.env.SITE_URL == PRODUCTION_URL
        ? [{ userAgent: '*', allow: ['/'] }]
        : [{ userAgent: '*', disallow: ['/'] }],
  },
};

const gatsbyPluginSitemap = {
  resolve: 'gatsby-plugin-sitemap',
  options: {
    output: '/sitemap.xml',
  },
};

const gatsbySourceDrupal = {
  resolve: 'gatsby-source-drupal',
  options: {
    baseUrl: process.env.DRUPAL_URL,
  },
};

const gatsbyPluginCreateClientPaths = {
  resolve: `gatsby-plugin-create-client-paths`,
  options: { prefixes: [`/app/*`] },
};

module.exports = {
  siteMetadata: {
    title: 'IntSaab 2021 Finland',
    description: 'International Saab Meeting 2021',
    keywords:
      'IntSaab, IntSaab2021, Saab, International, Himos, Finland, August',
    siteUrl: process.env.SITE_URL,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    `gatsby-plugin-sass`,
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    gatsbySourceFilesystem,
    gatsbyPluginManifest,
    gatsbyPluginRobotsTxt,
    gatsbyPluginSitemap,
    gatsbySourceDrupal,
    gatsbyPluginCreateClientPaths,
  ],
};
