
site_url="${1:-}"

if [ -z $site_url ]; then
    echo "Usage: $0 <site_url>"
    exit 1
fi

set -eux -o pipefail

site_url=$(echo $site_url | cut -d '/' -f 3)

dist_id=$(aws cloudfront list-distributions | \
    jq -r --arg url $site_url '.DistributionList.Items[] | select(.Aliases.Items[] | contains($url)) | .Id')

[ -n "$dist_id" ]

aws cloudfront create-invalidation \
    --distribution-id $dist_id \
    --paths '/*'
