AWSTemplateFormatVersion: 2010-09-09
Description: Cloud infrastructure for IntSaab2021.com

Parameters:
  DeploymentEnvironment:
    Type: String
    Default: dev
    AllowedValues:
      - dev
      - staging
      - prod
    Description: Please enter dev, staging or prod. Default is dev.

Mappings:
  EnvironmentTargets:
    dev:
      bucketName: '{{resolve:ssm:/IntSaab2021/Dev/Bucket/Name:1}}'
      bucketPath: '{{resolve:ssm:/IntSaab2021/Dev/Bucket/Path:1}}'
      certArn: '{{resolve:ssm:/IntSaab2021/Dev/CertArn:2}}'
      site: '{{resolve:ssm:/IntSaab2021/Dev/Site:2}}'
      zone: 'Z03831834TREH4UYD4WZ'
    staging:
      bucketName: '{{resolve:ssm:/IntSaab2021/Staging/Bucket/Name:1}}'
      bucketPath: '{{resolve:ssm:/IntSaab2021/Staging/Bucket/Path:1}}'
      certArn: '{{resolve:ssm:/IntSaab2021/Staging/CertArn:2}}'
      site: '{{resolve:ssm:/IntSaab2021/Staging/Site:2}}'
      zone: 'Z04051072ODXSNZV4OPRI'
    prod:
      bucketName: '{{resolve:ssm:/IntSaab2021/Prod/Bucket/Name:1}}'
      bucketPath: '{{resolve:ssm:/IntSaab2021/Prod/Bucket/Path:1}}'
      certArn: '{{resolve:ssm:/IntSaab2021/Prod/CertArn:1}}'
      site: '{{resolve:ssm:/IntSaab2021/Prod/Site:1}}'
      zone: 'Z04813622A9M5THB2BX2W'

Conditions:
  ProdResources: !Equals [!Ref DeploymentEnvironment, prod]

Resources:
  ############################################
  # DNS Hosted Zones are in the static stack
  ############################################

  ################################
  # DNS Resource Records
  ################################
  ResourceRecordIntsaab2021Com:
    Type: AWS::Route53::RecordSet
    Properties:
      AliasTarget:
        DNSName: !GetAtt CloudFrontIntSaab2021Com.DomainName
        HostedZoneId: Z2FDTNDATAQYW2 # fixed, see https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-route53-aliastarget.html
      HostedZoneId:
        !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, zone]
      Name: !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, site]
      Type: A

  ################################
  # CloudFront CDN distributions
  ################################
  CloudFrontIntSaab2021Com:
    Type: AWS::CloudFront::Distribution
    Properties:
      DistributionConfig:
        Aliases:
          - !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, site]
        Comment:
          Fn::Sub: 'IntSaab2021 ${DeploymentEnvironment}'
        CustomErrorResponses:
          - ErrorCode: 403
            ResponseCode: 200
            ResponsePagePath: '/index.html'
          - ErrorCode: 404
            ResponseCode: 200
            ResponsePagePath: '/index.html'
        DefaultCacheBehavior:
          ForwardedValues:
            QueryString: false
          TargetOriginId: !Join ['-', ['Origin', !Ref DeploymentEnvironment]]
          ViewerProtocolPolicy: redirect-to-https
          LambdaFunctionAssociations:
            - EventType: origin-request
              LambdaFunctionARN: !If
                - ProdResources
                - 'arn:aws:lambda:us-east-1:642673962243:function:serverlessrepo-standard-r-StandardRedirectsForClou-1NB7CBH4584QN:1'
                - 'arn:aws:lambda:us-east-1:471482825679:function:serverlessrepo-standard-r-StandardRedirectsForClou-LQEVK1E45CCZ:1'
        DefaultRootObject: index.html
        Enabled: true
        Logging:
          !If [
            ProdResources,
            Bucket: !GetAtt CloudFrontLogBucket.DomainName,
            !Ref 'AWS::NoValue',
          ]
        Origins:
          - DomainName:
              !FindInMap [
                EnvironmentTargets,
                !Ref DeploymentEnvironment,
                bucketPath,
              ]
            Id: !Join ['-', ['Origin', !Ref DeploymentEnvironment]]
            S3OriginConfig:
              OriginAccessIdentity:
                !Join [
                  '/',
                  [
                    'origin-access-identity/cloudfront',
                    !ImportValue IntSaab2021-shared-OaiId,
                  ],
                ]
        PriceClass: PriceClass_100
        ViewerCertificate:
          AcmCertificateArn:
            !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, certArn]
          SslSupportMethod: sni-only
      Tags:
        - Key: 'cost-booking'
          Value: 'intsaab2021'

  ################################
  # Policies
  ################################
  # Grant bucket read permission to OAI user ID
  S3AllowOaiReadAccessPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket:
        !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, bucketName]
      PolicyDocument:
        Statement:
          - Action:
              - 's3:GetObject'
            Sid: 'Grant a CloudFront Origin Identity access to support private content'
            Effect: Allow
            Principal:
              CanonicalUser: !ImportValue IntSaab2021-shared-OaiUser
            Resource:
              Fn::Join:
                - ''
                - - 'arn:aws:s3:::'
                  - !FindInMap [
                      EnvironmentTargets,
                      !Ref DeploymentEnvironment,
                      bucketName,
                    ]
                  - '/*'
    DependsOn: S3BucketIntSaab2021Com

  ################################
  # Buckets
  ################################
  S3BucketIntSaab2021Com:
    Type: AWS::S3::Bucket
    Properties:
      BucketName:
        !FindInMap [EnvironmentTargets, !Ref DeploymentEnvironment, bucketName]
      Tags:
        - Key: 'cost-booking'
          Value: 'intsaab2021'

  CloudFrontLogBucket:
    Type: AWS::S3::Bucket
    Condition: ProdResources
    Properties:
      BucketName: cloudfront-logs.intsaab2021.com
      Tags:
        - Key: 'cost-booking'
          Value: 'intsaab2021'
