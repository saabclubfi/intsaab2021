
# Dev deployment

Shared assets:

`AWS_DEFAULT_REGION=eu-north-1 aws cloudformation deploy --template-file static-assets-dev-account.template.yml --stack-name IntSaab2021-shared`

Environment specific assets:

`AWS_DEFAULT_REGION=eu-north-1 aws cloudformation deploy --template-file environments.template.yml --parameter-overrides DeploymentEnvironment=dev --stack-name intsaab2021-dev`

`AWS_DEFAULT_REGION=eu-north-1 aws cloudformation deploy --template-file environments.template.yml --parameter-overrides DeploymentEnvironment=staging --stack-name intsaab2021-staging`

# Production deployment

Shared assets:

`AWS_DEFAULT_REGION=eu-north-1 aws cloudformation deploy --profile prodaccess --template-file static-assets-prod-account.template.yml --stack-name IntSaab2021-shared`

Environment specific assets:

`AWS_DEFAULT_REGION=eu-north-1 aws cloudformation deploy  --profile prodaccess --template-file environments.template.yml --parameter-overrides DeploymentEnvironment=prod --stack-name intsaab2021-prod`
