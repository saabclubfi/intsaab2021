AWSTemplateFormatVersion: 2010-09-09
Description: Cloud infrastructure for IntSaab2021.com


Resources:
################################
# DNS Hosted Zones
################################
  HostedZoneIntsaab2021Com:
    Type: "AWS::Route53::HostedZone"
    Properties:
      HostedZoneConfig:
        Comment: "Placeholder for additional comments"
      HostedZoneTags:
      - Key: "cost-booking"
        Value: "intsaab2021"
      Name: intsaab2021.com

  HostedZoneIntsaab2021Fi:
    Type: "AWS::Route53::HostedZone"
    Properties:
      HostedZoneConfig:
        Comment: "Placeholder for additional comments"
      HostedZoneTags:
      - Key: "cost-booking"
        Value: "intsaab2021"
      Name: intsaab2021.fi

################################
# DNS Resource records
################################
  # For redirecting from intsaab2021.fi to intsaab2021.com
  ResourceRecordIntsaab2021FiApex:
    Type: AWS::Route53::RecordSet
    Properties:
      AliasTarget:
        DNSName: s3-website.eu-north-1.amazonaws.com # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
        HostedZoneId: Z3BAZG2TWCNX0D # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
      HostedZoneId: !Ref HostedZoneIntsaab2021Fi
      Name: intsaab2021.fi
      Type: A

  ResourceRecordIntsaab2021FiWww:
    Type: AWS::Route53::RecordSet
    Properties:
      AliasTarget:
        DNSName: s3-website.eu-north-1.amazonaws.com # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
        HostedZoneId: Z3BAZG2TWCNX0D # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
      HostedZoneId: !Ref HostedZoneIntsaab2021Fi
      Name: www.intsaab2021.fi
      Type: A

  ResourceRecordIntsaab2021ComWww:
    Type: AWS::Route53::RecordSet
    Properties:
      AliasTarget:
        DNSName: s3-website.eu-north-1.amazonaws.com # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
        HostedZoneId: Z3BAZG2TWCNX0D # fixed, from https://docs.aws.amazon.com/general/latest/gr/rande.html
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: www.intsaab2021.com
      Type: A

  # Google verification codes
  ResourceRecordIntsaab2021ComGoogleVerification:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: GSuite and Search Console domain verification codes
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Type: TXT
      Name: intsaab2021.com
      TTL: '3600'
      ResourceRecords:
        # Google Suite
        - '"google-site-verification=TEJU07vSRFIZ_Ovigis8-fzNjvOrU5WUcQ_HeQWTJGw"'
        # Search Console
        - '"google-site-verification=4w_0xvSdy9yN7xDF-boVZ81tPYEBhB7NG47LHXFJFAM"'

  # Google suite MX records
  ResourceRecordIntsaab2021ComGsuiteMailTransfer:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: GSuite MX records
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Type: MX
      Name: intsaab2021.com
      TTL: '3600'
      ResourceRecords:
        - '1 ASPMX.L.GOOGLE.COM'
        - '5 ALT1.ASPMX.L.GOOGLE.COM'
        - '5 ALT2.ASPMX.L.GOOGLE.COM'
        - '10 ALT3.ASPMX.L.GOOGLE.COM'
        - '10 ALT4.ASPMX.L.GOOGLE.COM'

  # SES verification record
  ResourceRecordIntsaab2021ComSesVerification:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: SES verification records for dev and prod account
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: _amazonses.intsaab2021.com
      Type: TXT
      TTL: '3600'
      ResourceRecords:
        # dev
        - '"eo2NMNlTAnBFnIqT8zU8SOA6SbUE8c7YEmMdN/wYgow="'
        # prod
        - '"g8Ekoswm3x/CgvbFFKahvXJkZrbMjmqehNgVMKriKe4="'

  # DKIM public keys for dev account SES
  ResourceRecordIntSaab2021ComSesDkimDevA:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Dev SES DKIM public key A
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: 454l67ho4axu42eoq24cgq2h2by4bwmj._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - '454l67ho4axu42eoq24cgq2h2by4bwmj.dkim.amazonses.com'
  ResourceRecordIntSaab2021ComSesDkimDevB:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Dev SES DKIM public key B
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: 7bftje2r3hzcibjqmouedk2xkf6f3no5._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - '7bftje2r3hzcibjqmouedk2xkf6f3no5.dkim.amazonses.com'
  ResourceRecordIntSaab2021ComSesDkimDevC:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Dev SES DKIM public key C
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: bqapy6debiq6ob33p4qxpiabm6s2kh7b._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - 'bqapy6debiq6ob33p4qxpiabm6s2kh7b.dkim.amazonses.com'

  # DKIM public keys for prod account SES
  ResourceRecordIntSaab2021ComSesDkimProdA:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Prod SES DKIM public key A
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: tdtebsv3klmnbnqirhs3462ijptsmb4f._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - 'tdtebsv3klmnbnqirhs3462ijptsmb4f.dkim.amazonses.com'
  ResourceRecordIntSaab2021ComSesDkimProdB:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Prod SES DKIM public key B
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: ucodp6xipinqfvpzxbcktagyld2ws7b5._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - 'ucodp6xipinqfvpzxbcktagyld2ws7b5.dkim.amazonses.com'
  ResourceRecordIntSaab2021ComSesDkimProdC:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Prod SES DKIM public key C
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Name: dxs2n6hihzwjwu2q7kbfmx2pt6z5okl6._domainkey.intsaab2021.com
      Type: CNAME
      TTL: '3600'
      ResourceRecords:
        - 'dxs2n6hihzwjwu2q7kbfmx2pt6z5okl6.dkim.amazonses.com'

  # Delegate dev.intsaab2021.com and staging.intsaab2021.com
  # to their respective name servers.
  ResourceRecordDevNameServers:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Dev subdomain name servers
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Type: NS
      Name: dev.intsaab2021.com
      TTL: '3600'
      ResourceRecords:
        - ns-1277.awsdns-31.org
        - ns-326.awsdns-40.com
        - ns-1962.awsdns-53.co.uk
        - ns-912.awsdns-50.net

  ResourceRecordStagingNameServers:
    Type: AWS::Route53::RecordSet
    Properties:
      Comment: Staging subdomain name servers
      HostedZoneId: !Ref HostedZoneIntsaab2021Com
      Type: NS
      Name: staging.intsaab2021.com
      TTL: '3600'
      ResourceRecords:
        - ns-649.awsdns-17.net
        - ns-1845.awsdns-38.co.uk
        - ns-1237.awsdns-26.org
        - ns-4.awsdns-00.com

################################
# CloudFront OAI
################################
  # Access to S3 buckets is through "Origin Access Identity" pseudo user
  # Sharing OAI between distributions is recommended.
  CloudFrontOAI:
    Type: AWS::CloudFront::CloudFrontOriginAccessIdentity
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: "OAI for IntSaab2021 CloudFront distributions"

################################
# Buckets
################################

  # This is probably the most straightforward way to do the redirections.
  # The same works for www.intsaab2021.fi and www.intsaab2021.com
  # S3 cannot handle https, though.
  S3BucketIntSaab2021Fi:
    Type: AWS::S3::Bucket
    Properties:
      AccessControl: PublicRead
      BucketName: intsaab2021.fi
      Tags:
        - Key: "cost-booking"
          Value: "intsaab2021"
      WebsiteConfiguration:
        RedirectAllRequestsTo:
          HostName: intsaab2021.com
          Protocol: https

  S3BucketIntSaab2021ComWww:
    Type: AWS::S3::Bucket
    Properties:
      AccessControl: PublicRead
      BucketName: www.intsaab2021.com
      Tags:
        - Key: "cost-booking"
          Value: "intsaab2021"
      WebsiteConfiguration:
        RedirectAllRequestsTo:
          HostName: intsaab2021.com
          Protocol: https

  S3BucketIntSaab2021FiWww:
    Type: AWS::S3::Bucket
    Properties:
      AccessControl: PublicRead
      BucketName: www.intsaab2021.fi
      Tags:
        - Key: "cost-booking"
          Value: "intsaab2021"
      WebsiteConfiguration:
        RedirectAllRequestsTo:
          HostName: intsaab2021.com
          Protocol: https

################################ end of resources ################################

Outputs:
  HostedZoneIntsaab2021Com:
    Description: Hosted zone ID for intsaab2021.com
    Value: !Ref HostedZoneIntsaab2021Com
    Export:
      Name: !Sub "${AWS::StackName}-HostedZoneIntsaab2021-prod"
  HostedZoneIntsaab2021Fi:
    Description: Hosted zone ID for intsaab2021.fi
    Value: !Ref HostedZoneIntsaab2021Fi
    Export:
      Name: !Sub "${AWS::StackName}-HostedZoneIntsaab2021Fi-prod"
  NameServersIntsaab2021Com:
    Description: Name servers for intsaab2021.com
    Value:
      'Fn::Join':
      - " "
      - 'Fn::GetAtt':
        - HostedZoneIntsaab2021Com
        - NameServers
  NameServersIntsaab2021Fi:
    Description: Name servers for intsaab2021.fi
    Value:
      'Fn::Join':
      - " "
      - 'Fn::GetAtt':
        - HostedZoneIntsaab2021Fi
        - NameServers
  OaiId:
    Description: OAI id
    Value: !Ref CloudFrontOAI
    Export:
      Name: !Sub "${AWS::StackName}-OaiId"
  OaiCanonicalUser:
    Description: OAI pseudo user id
    Value: !GetAtt CloudFrontOAI.S3CanonicalUserId
    Export:
      Name: !Sub "${AWS::StackName}-OaiUser"

