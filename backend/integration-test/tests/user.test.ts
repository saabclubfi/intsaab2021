import {
  CognitoIdentityProviderClientConfig,
  CognitoIdentityProvider,
} from '@aws-sdk/client-cognito-identity-provider';
import KSUID from 'ksuid';
import got from 'got';

const env = process.env.DEPLOYMENT_ENVIRONMENT || 'dev';
const endpoint =
  process.env.API_ENDPOINT || `https://api.${env}.intsaab2021.com/v1`;
const userPoolId = process.env.USER_POOL_ID || 'eu-west-1_iZykdXnQl';
const appClientId =
  process.env.USER_POOL_CLIENT_ID || '7kh2rha8g405ibiu1vn8h8a8q4';

const cognitoConfig: CognitoIdentityProviderClientConfig = {
  region: process.env.AWS_REGION || 'eu-west-1',
};
const client = new CognitoIdentityProvider(cognitoConfig);
const id = KSUID.randomSync().string;
const testUser = `test-${id}@example.com`;
const testPass = `pass-${id}`;

let token: string;

beforeAll(async () => {
  await client.signUp({
    ClientId: appClientId,
    Username: testUser,
    Password: testPass,
  });

  await client.adminConfirmSignUp({
    UserPoolId: userPoolId,
    Username: testUser,
  });

  const response = await client.initiateAuth({
    AuthFlow: 'USER_PASSWORD_AUTH',
    ClientId: appClientId,
    AuthParameters: {
      USERNAME: testUser,
      PASSWORD: testPass,
    },
  });

  token = response.AuthenticationResult.AccessToken;
});

afterAll(async () => {
  return client.adminDeleteUser({
    UserPoolId: userPoolId,
    Username: testUser,
  });
});

describe('User management', () => {
  test('Create and get user', async () => {
    // Create user through the API
    const response = await got.post(endpoint + '/user', {
      headers: getHeaders(),
      json: userFixture,
      timeout: 5000,
    });

    // Expect successful and correct response
    expect(response.statusCode).toBe(200);
    expect(response.body).toBe('{}');

    // Read back the user
    const getResponse = await got.get(endpoint + '/user', {
      headers: getHeaders(),
      timeout: 5000,
    });

    // Expect successful and correct response
    expect(getResponse.statusCode).toBe(200);
    const returnedUser = JSON.parse(getResponse.body);

    expect(returnedUser).toMatchObject({ data: userFixture });
  });
});

describe('Participant management', () => {
  test('Create and get participants', async () => {
    const gotOptions = {
      headers: getHeaders(),
      timeout: 5000,
    };

    // Create a participant
    await got.post(endpoint + '/user/participant', {
      ...gotOptions,
      json: participantFixture,
    });

    // Get participants and expect to find the created one
    const getResponse = await got.get(endpoint + '/user/participant', {
      ...gotOptions,
    });
    expect(getResponse.statusCode).toBe(200);

    const participants = JSON.parse(getResponse.body).data;
    expect(participants).toHaveLength(1);
    const { id, ...participant } = participants[0];
    expect(participant).toMatchObject(participantFixture);

    // Delete it
    await got.delete(endpoint + `/user/participant/${id}`, {
      ...gotOptions,
    });

    // Expect get now returns nothing
    const { data } = await got.get(endpoint + '/user/participant', {
      ...gotOptions,
      responseType: 'json',
      resolveBodyOnly: true,
    });
    expect(data).toHaveLength(0);
  });

  test('Update participant', async () => {
    const gotOptions = {
      headers: getHeaders(),
      timeout: 5000,
    };

    // Create two participants
    const postResponses = await Promise.all([
      got.post(endpoint + '/user/participant', {
        ...gotOptions,
        json: participantFixture,
        responseType: 'json',
        resolveBodyOnly: true,
      }),
      got.post(endpoint + '/user/participant', {
        ...gotOptions,
        json: participantFixture,
        responseType: 'json',
        resolveBodyOnly: true,
      }),
    ]);

    const [id1, id2] = postResponses.map((resp: any) => resp.data.id);

    // Update one of them
    await got.put(endpoint + `/user/participant/${id2}`, {
      ...gotOptions,
      json: {
        ...participantFixture,
        name: 'Updated ParticipantName',
      },
    });

    // Get participants and expect there still to be only two
    const getResponse = await got.get(endpoint + '/user/participant', {
      ...gotOptions,
    });
    expect(getResponse.statusCode).toBe(200);

    const participants: [TestObject] = JSON.parse(getResponse.body).data;
    expect(participants).toHaveLength(2);

    // Expect one of them was updated with new name
    expect(
      participants.filter(
        (participant) => participant.name == 'Updated ParticipantName',
      ),
    ).toHaveLength(1);

    // Delete both participants
    await Promise.all([
      got.delete(endpoint + `/user/participant/${id1}`, {
        ...gotOptions,
      }),
      got.delete(endpoint + `/user/participant/${id2}`, {
        ...gotOptions,
      }),
    ]);

    // Expect Get to return nothing
    const { data } = await got.get(endpoint + '/user/participant', {
      ...gotOptions,
      responseType: 'json',
      resolveBodyOnly: true,
    });
    expect(data).toHaveLength(0);
  });
});

describe('Payment handling', () => {
  test('Create payment', async () => {
    // Create user through the API
    let response = await got.post(endpoint + '/user', {
      headers: getHeaders(),
      json: userFixture,
      timeout: 5000,
    });

    // Expect successful and correct response
    expect(response.statusCode).toBe(200);
    expect(response.body).toBe('{}');

    response = await got.post(endpoint + '/payment/checkoutFi', {
      headers: getHeaders(),
      timeout: 5000,
    });

    expect(response.statusCode).toBe(200);
    expect(response.body).toBeDefined();

    const { data, error } = JSON.parse(response.body);
    expect(error).toBeUndefined();
    expect(data).toBeDefined();

    expect(data.providers).toBeDefined();
    expect(data.providers.length).toBeGreaterThanOrEqual(1);
    expect(data.stamp).toBeDefined();
    expect(data.transactionId).toBeDefined();
  });

  test('List payment providers', async () => {
    let response = await got.get(endpoint + '/payment/checkoutFi', {
      headers: getHeaders(),
      timeout: 5000,
    });

    expect(response.statusCode).toBe(200);
    expect(response.body).toBeDefined();

    const { data, error } = JSON.parse(response.body);
    expect(error).toBeUndefined();
    expect(data).toBeDefined();
    expect(data.length).toBeGreaterThanOrEqual(1);
  });

  test('Get cart', async () => {
    // Create user through the API
    const response = await got.post(endpoint + '/user', {
      headers: getHeaders(),
      json: userFixture,
      timeout: 5000,
    });

    // Expect successful and correct response
    expect(response.statusCode).toBe(200);
    expect(response.body).toBe('{}');

    // Is it bad to perform minor side effects in map()? :)
    const participantIds = ['18', '12', '2'].map(async (age) => {
      let participantResponse: any = await got.post(
        endpoint + '/user/participant',
        {
          headers: getHeaders(),
          json: {
            ...participantFixture,
            age,
          },
          timeout: 5000,
          responseType: 'json',
          resolveBodyOnly: true,
        },
      );

      // Expect successful and correct response
      expect(participantResponse.data).toBeDefined();
      return participantResponse.data.id;
    });

    // Sometimes we're too fast for DynamoDB as the Lambda does normal
    // eventually consistent reads.
    await new Promise((resolve) => setTimeout(resolve, 100));

    try {
      let response = await got.get(endpoint + '/cart', {
        headers: getHeaders(),
        timeout: 5000,
      });
      expect(response.statusCode).toBe(200);
      expect(response.body).toBeDefined();

      const { data, error } = JSON.parse(response.body);
      expect(error).toBeUndefined();
      expect(data).toBeDefined();
      expect(data.totalPrice).toBe(33500);
      expect(data.user.unitPrice).toBe(14500);
      expect(data.participants.length).toBe(3);
      const numericAscending = (a: number, b: number) => a - b;
      expect(
        data.participants.map((p: any) => p.age).sort(numericAscending),
      ).toEqual([2, 12, 18]);
      expect(
        data.participants.map((p: any) => p.unitPrice).sort(numericAscending),
      ).toEqual([0, 4500, 14500]);
    } finally {
      // Remove participants
      await Promise.all(
        participantIds.map((id) =>
          got.delete(endpoint + `/user/participant/${id}`, {
            headers: getHeaders(),
            timeout: 5000,
          }),
        ),
      );
    }
  });
});

const getHeaders = (): Record<string, string> => ({
  Authorization: `Bearer ${token}`,
  Accept: 'application/json',
  'Content-Type': 'application/json',
});

const userFixture: TestObject = {
  name: 'Testi Useri',
  phone: '555101010',
  email: 'foo@bar.fi',
  address: {
    line1: 'FirstRow',
    line2: 'SecondRow',
    city: 'BigCity',
    zip: '02020',
    region: 'region',
    country: 'FI',
  },
  car: 'Tsaappi 9-5 eiku 9^5',
  subscriptions: ['new_posts'],
};

const participantFixture: TestObject = {
  name: 'Test Participant',
  phone: '555101010',
  email: 'foo@bar.fi',
  age: '22',
  address: {
    line1: 'FirstRow',
    line2: 'SecondRow',
    city: 'BigCity',
    zip: '02020',
    region: 'region',
    country: 'FI',
  },
};

interface TestObject {
  id?: string;
  name: string;
  [key: string]: string | object;
}
