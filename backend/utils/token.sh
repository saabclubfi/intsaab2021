#!/bin/bash

read -p 'Email: ' user
read -sp "Password: " passwd

AWS_REGION=eu-west-1 aws cognito-idp initiate-auth \
    --auth-flow USER_PASSWORD_AUTH --client-id 7kh2rha8g405ibiu1vn8h8a8q4 \
    --auth-parameters USERNAME=$user,PASSWORD=$passwd | egrep '(Access|Refresh)Token'

