process.env.DDB_TABLE_NAME = 'user-test';

import { APIGatewayProxyEventV2, Context } from 'aws-lambda';
import { v4 as uuidv4 } from 'uuid';
import { DynamoDB } from 'aws-sdk';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import nock from 'nock';
import KSUID from 'ksuid';

import {
  getUser,
  postParticipant,
  postUser,
  getParticipants,
  putParticipant,
  deleteParticipant,
  getCart,
  createPayment,
  checkoutCallback,
  getPaymentProviders,
} from '../src/index';
import { Cart } from '../src/interfaces';
import { clearCache } from '../src/config';

const config = {
  convertEmptyValues: true,
  endpoint: process.env.DDB_LOCAL || 'http://localhost:8000',
  sslEnabled: false,
  region: 'local-env',
};

const testTableName = process.env.DDB_TABLE_NAME;

process.env.DEPLOYMENT_ENVIRONMENT =
  process.env.DEPLOYMENT_ENVIRONMENT || 'dev';
const SSM_PATH_PREFIX = `/IntSaab2021/${process.env.DEPLOYMENT_ENVIRONMENT}`;
process.env.AWS_DEFAULT_REGION = 'eu-north-1';
process.env.AWS_ACCESS_KEY_ID = 'access-key';
process.env.AWS_SECRET_ACCESS_KEY = 'secret-key';

const ddb = new DocumentClient(config);
const ddbLowLevelClient = new DynamoDB(config);

beforeEach(async () => {
  return createTestTable(testTableName);
});

afterEach(async () => {
  nock.cleanAll();
  return deleteTestTable(testTableName);
});

describe('User tests', () => {
  test('Get user happy', async () => {
    const newDbItem = {
      PK: `${testUser}#USER`,
      SK: 'USER',
      ...testUserItem,
    };

    await ddb
      .put({
        TableName: testTableName,
        Item: newDbItem,
      })
      .promise();

    const { data } = await getUser(testRequest('GET'), testContext);
    expect(data).toMatchObject(testUserItem);
  });

  test('Get user dynamo throws', async () => {
    const scope = nock(config.endpoint, {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .persist() // to keep the setup throughout all boto3 retries
      .post(() => true) // any URI
      .replyWithError({
        __type: 'com.amazonaws.dynamodb.v20120810#InternalServerError',
        message: 'Other DynamoDB error occurred',
      });

    const { data, error } = await getUser(testRequest('GET'), testContext);
    expect(data).toBeUndefined();
    expect(error).toBe('General error');
  });

  test('Get user dynamo out of capacity', async () => {
    const scope = nock(config.endpoint, {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .persist()
      .post(() => true)
      .replyWithError({
        __type:
          'com.amazonaws.dynamodb.v20120810#ProvisionedThroughputExceededException',
        message: 'DynamoDB out of provisioned capacity',
      });

    const { data, error } = await getUser(testRequest('GET'), testContext);
    expect(data).toBeUndefined();
    expect(error).toBe('General error');
  });

  test('Post user happy', async () => {
    const request = testRequest('POST');
    request.body = JSON.stringify(testUserItem);
    const response = await postUser(request, testContext);

    // Expect stuff to be in DDB
    const {
      Item: { PK, SK, ...user },
    } = await ddb
      .get({
        TableName: testTableName,
        Key: {
          PK: `${testUser}#USER`,
          SK: 'USER',
        },
      })
      .promise();

    expect(user).toMatchObject(testUserItem);
  });
});

describe('Participant tests', () => {
  test('Post participant happy', async () => {
    const request = testRequest('POST');
    request.body = JSON.stringify(testParticipantItem);
    const response = await postParticipant(request, testContext);

    const returned_id = response.data.id;

    // Expect stuff to be in DDB
    const {
      Item: { PK, SK, ...user },
    } = await ddb
      .get({
        TableName: testTableName,
        Key: {
          PK: `${testUser}#USER`,
          SK: `PARTICIPANT#${returned_id}`,
        },
      })
      .promise();

    expect(user).toMatchObject(testParticipantItem);
  });

  test('Get participants happy', async () => {
    const id1 = (await KSUID.random()).string;
    const id2 = (await KSUID.random()).string;
    const newDbItem1 = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id1}`,
      ...testParticipantItem,
    };
    const newDbItem2 = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id2}`,
      ...testParticipantItem,
    };

    await Promise.all([
      ddb
        .put({
          TableName: testTableName,
          Item: newDbItem1,
        })
        .promise(),

      ddb
        .put({
          TableName: testTableName,
          Item: newDbItem2,
        })
        .promise(),
    ]);

    const getRequest = testRequest('GET');
    const { data } = await getParticipants(getRequest, testContext);
    expect(data).toHaveLength(2);
    expect(data[0]).toMatchObject(testParticipantItem);
    expect(data[1]).toMatchObject(testParticipantItem);
  });

  test('Put participant happy', async () => {
    const id = (await KSUID.random()).string;
    const newDbItem = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id}`,
      ...testParticipantItem,
    };

    await ddb
      .put({
        TableName: testTableName,
        Item: newDbItem,
      })
      .promise();

    const request = testRequest('PUT');
    const item = { ...testParticipantItem };
    item.age = '1';
    item.email = 'new@ema.il';
    request.body = JSON.stringify(item);
    request.pathParameters = {
      id,
    };
    await putParticipant(request, testContext);

    // Expect stuff to be in DDB
    const {
      Item: { PK, SK, ...user },
    } = await ddb
      .get({
        TableName: testTableName,
        Key: {
          PK: `${testUser}#USER`,
          SK: `PARTICIPANT#${id}`,
        },
      })
      .promise();

    expect(user).toMatchObject(item);
  });

  test('Put participant fails if does not exist', async () => {
    const id = (await KSUID.random()).string;

    const request = testRequest('PUT');
    request.body = JSON.stringify(testParticipantItem);
    request.pathParameters = {
      id,
    };

    const response = await putParticipant(request, testContext);

    // Expect stuff to not be in DDB
    const queryResponse = await ddb
      .query({
        TableName: testTableName,
        KeyConditionExpression: 'PK = :pk AND begins_with(SK, :sk)',
        ExpressionAttributeValues: {
          ':pk': `${testUser}#USER`,
          ':sk': 'PARTICIPANT#',
        },
      })
      .promise();

    expect(response?.error).toBeDefined();
    expect(queryResponse.Items.length).toBe(0);
  });

  test('Delete participant happy', async () => {
    const id = (await KSUID.random()).string;
    const newDbItem = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id}`,
      ...testParticipantItem,
    };

    await ddb
      .put({
        TableName: testTableName,
        Item: newDbItem,
      })
      .promise();

    const request = testRequest('DELETE');
    request.pathParameters = {
      id,
    };
    await deleteParticipant(request, testContext);

    // Expect stuff to not be in DDB
    const response = await ddb
      .get({
        TableName: testTableName,
        Key: {
          PK: `${testUser}#USER`,
          SK: `PARTICIPANT#${id}`,
        },
      })
      .promise();

    expect(response.Item).toBeUndefined;
  });

  test('Delete participant fails if does not exist', async () => {
    const id = (await KSUID.random()).string;

    const request = testRequest('DELETE');
    request.pathParameters = {
      id,
    };
    const response = await deleteParticipant(request, testContext);

    expect(response?.error).toBeDefined();
  });
});

describe('Cart tests', () => {
  test('Get cart happy', async () => {
    const userItem = {
      PK: `${testUser}#USER`,
      SK: 'USER',
      ...testUserItem,
    };

    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, {
        Parameters: [],
      });

    const id1 = (await KSUID.random()).string;
    const id2 = (await KSUID.random()).string;

    const participant0 = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id1}`,
      ...testParticipantItem,
    };
    const participant1 = {
      PK: `${testUser}#USER`,
      SK: `PARTICIPANT#${id2}`,
      ...testParticipantItem,
    };

    await Promise.all(
      [userItem, participant0, participant1].map((item) =>
        ddb
          .put({
            TableName: testTableName,
            Item: item,
          })
          .promise(),
      ),
    );

    const response = await getCart(testRequest('GET'), testContext);

    expect(response?.data).toBeDefined();
  });

  test('Correct cart total is calculated', async () => {
    clearCache();

    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, {
        Parameters: [
          {
            Name: '/IntSaab2021/dev/Price/User',
            Value: 10000,
          },
          {
            Name: '/IntSaab2021/dev/Price/Participant',
            Value: 10000,
          },
          {
            Name: '/IntSaab2021/dev/Price/Participant/18',
            Value: 8000,
          },
          {
            Name: '/IntSaab2021/dev/Price/Participant/8',
            Value: 6000,
          },
          {
            Name: '/IntSaab2021/dev/Price/Participant/3',
            Value: 0,
          },
        ],
      });

    const user0 = {
      PK: `${testUser}#USER`,
      SK: 'USER',
      ...testUserItem, // mocked to 100 eur
    };

    const participant0 = {
      ...testParticipantItem,
      age: '15', // 80 eur
    };
    const participant1 = {
      ...testParticipantItem,
      age: '2', // 0 eur
    };
    const participant2 = {
      ...testParticipantItem,
      age: '18', // 100 eur
    };

    await Promise.all(
      [user0, participant0, participant1, participant2].map(
        async (item, index) =>
          ddb
            .put({
              TableName: testTableName,
              Item: {
                PK: `${testUser}#USER`,
                SK: `PARTICIPANT#${
                  (await KSUID.random(Date.now() + 1000 * index)).string
                }`, // increase timestamp to get correct SK order
                ...item,
              },
            })
            .promise(),
      ),
    );

    process.env.DEPLOYMENT_ENVIRONMENT = 'dev';
    const response = await getCart(testRequest('GET'), testContext);

    expect(response?.data).toBeDefined();

    const { user, participants, totalPrice } = response.data as Cart;

    expect(user.unitPrice).toBe(10000);
    expect(participants[0].unitPrice).toBe(8000);
    expect(participants[0].age).toBe(15);
    expect(participants[1].unitPrice).toBe(0);
    expect(participants[1].age).toBe(2);
    expect(totalPrice).toBe(28000);
  });

  test('Cart returns totalPrice 0 with no user config', async () => {
    const response = await getCart(testRequest('GET'), testContext);

    expect(response?.data).toBeDefined();

    const { user, participants, totalPrice } = response.data as Cart;

    expect(user).toBeUndefined();
    expect(participants).toStrictEqual([]);
    expect(totalPrice).toBe(0);
  });
});

describe('checkout', () => {
  test('createPayment happy', async () => {
    const request = testRequest('POST');

    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, {
        Parameters: [
          {
            Name: `${SSM_PATH_PREFIX}/Price/User`,
            Value: '10000',
          },
        ],
      });

    nock('https://api.checkout.fi', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post('/payments')
      .reply(200, testCheckoutPaymentResponse);

    // make sure user is in DDB
    await ddb
      .put({
        TableName: testTableName,
        Item: {
          PK: `${testUser}#USER`,
          SK: 'USER',
          ...testUserItem,
        },
      })
      .promise();

    const response = await createPayment(request, testContext);

    const {
      Item: { PK, SK, ...payment },
    } = await ddb
      .get({
        TableName: testTableName,
        Key: {
          PK: `${testUser}#PAYMENT`,
          SK: `PAYMENT#${response.data.stamp}`,
        },
      })
      .promise();

    expect(payment.data['checkout-transaction-id']).toBe(
      testCheckoutPaymentResponse.transactionId,
    );
    expect(response.data).toStrictEqual({
      ...testCheckoutPaymentResponse,
      stamp: response.data.stamp,
    });
  });

  test('getPaymentProviders happy', async () => {
    const request = testRequest('POST');

    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, { Parameters: [] });

    nock('https://api.checkout.fi', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .get('/merchants/payment-providers')
      .reply(200, testCheckoutPaymentProvidersResponse);

    const response = await getPaymentProviders(request, testContext);
    expect(response.data).toStrictEqual(testCheckoutPaymentProvidersResponse);
  });

  test('callback signature calculation should succeed', async () => {
    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, { Parameters: [] });

    const request = testRequest('GET');
    request.queryStringParameters = {
      ...testCheckoutCallbackQueryStringParameters,
    };
    const response = await checkoutCallback(request, testContext);
    expect(response.statusCode).toBe(200);
  });

  test('callback signature calculation should fail', async () => {
    nock('https://ssm.eu-north-1.amazonaws.com:443', {
      allowUnmocked: true,
      encodedQueryParams: true,
    })
      .post(() => true)
      .reply(200, { Parameters: [] });

    const request = testRequest('GET');
    request.queryStringParameters = {
      ...testCheckoutCallbackQueryStringParameters,
      signature: 'INVALID',
    };
    const response = await checkoutCallback(request, testContext);
    expect(response.statusCode).toBe(200);
  });
});

const testUser = uuidv4();

const testUserItem = {
  name: 'Testi Kayttaja',
  phone: '+3585552020',
  address: {
    line1: 'TestAddress 1',
    city: 'TestCity',
    zip: '20202',
    country: 'FI',
  },
  car: 'Cool Saab',
  subscriptions: ['test'],
  email: 'test.user@test.doma.in',
};

const testParticipantItem = {
  name: 'Testi Seuralainen',
  phone: '+3585551111',
  address: {
    line1: 'TestAddress 2',
    city: 'TestCity',
    zip: '20202',
    country: 'FI',
  },
  age: '19',
  email: 'test.participant@test.doma.in',
};

const testRequest = (method: string): APIGatewayProxyEventV2 => ({
  headers: {
    accept: 'application/json',
  },
  isBase64Encoded: false,
  version: '2.0',
  routeKey: `${method} /user`,
  rawPath: '/v1/user',
  rawQueryString: '',
  requestContext: {
    accountId: 'test-account-id',
    apiId: 'aaaaa',
    authorizer: {
      jwt: {
        claims: {
          sub: testUser,
        },
        scopes: null,
      },
    },
    domainName: 'aaaaa.execute-api.eu-north-1.amazonaws.com',
    domainPrefix: 'aaaaa',
    http: {
      method: method,
      path: '/v1/user',
      protocol: '',
      sourceIp: '',
      userAgent: '',
    },
    requestId: '',
    routeKey: `${method} /user`,
    stage: 'v1',
    time: '21/Nov/2020:19:48:50 +0000',
    timeEpoch: 1605988130957,
  },
});

const testContext: Context = {
  awsRequestId: '',
  callbackWaitsForEmptyEventLoop: true,
  functionName: '',
  functionVersion: '',
  invokedFunctionArn: '',
  logGroupName: '',
  logStreamName: '',
  memoryLimitInMB: '128',
  getRemainingTimeInMillis: () => 1,
  done: () => true,
  fail: () => true,
  succeed: () => true,
};

function createTestTable(tableName: string) {
  return ddbLowLevelClient
    .createTable({
      TableName: tableName,
      AttributeDefinitions: [
        { AttributeName: 'PK', AttributeType: 'S' },
        { AttributeName: 'SK', AttributeType: 'S' },
      ],
      KeySchema: [
        { AttributeName: 'PK', KeyType: 'HASH' },
        { AttributeName: 'SK', KeyType: 'RANGE' },
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    })
    .promise();
}

function deleteTestTable(tableName: string) {
  return ddbLowLevelClient
    .deleteTable({
      TableName: tableName,
    })
    .promise();
}

const testCheckoutPaymentProvidersResponse = {
  terms:
    'By continuing with your payment, you agree to our <a href="https://www.checkout.fi/ehdot-ja-sopimukset/maksuehdot" target="_blank">payment terms & conditions</a>',
  groups: [
    {
      id: 'mobile',
      name: 'Mobile payment methods',
      icon: 'https://payment.checkout.fi/static/img/payment-groups/mobile.png',
      svg: 'https://payment.checkout.fi/static/img/payment-groups/mobile.svg',
    },
  ],
  providers: [
    {
      url: 'https://maksu.pivo.fi/api/payments',
      icon: 'https://payment.checkout.fi/static/img/pivo_140x75.png',
      svg:
        'https://payment.checkout.fi/static/img/payment-methods/pivo-siirto.svg',
      name: 'Pivo',
      group: 'mobile',
      id: 'pivo',
      parameters: [
        {
          name: 'amount',
          value: 'base64 MTUyNQ==',
        },
      ],
    },
  ],
};

const testCheckoutPaymentResponse = {
  ...testCheckoutPaymentProvidersResponse,
  transactionId: '5770642a-9a02-4ca2-8eaa-cc6260a78eb6',
  href: 'https://api.checkout.fi/pay/5770642a-9a02-4ca2-8eaa-cc6260a78eb6',
  reference: '809759248',
};

const testCheckoutCallbackQueryStringParameters = {
  'checkout-account': '375917',
  'checkout-algorithm': 'sha256',
  'checkout-amount': '2964',
  'checkout-stamp': '15336332710015',
  'checkout-reference': '192387192837195',
  'checkout-transaction-id': '4b300af6-9a22-11e8-9184-abb6de7fd2d0',
  'checkout-status': 'ok',
  'checkout-provider': 'nordea',
  signature: 'b2d3ecdda2c04563a4638fcade3d4e77dfdc58829b429ad2c2cb422d0fc64080',
};
