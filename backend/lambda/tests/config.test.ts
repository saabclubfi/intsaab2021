import nock from 'nock';

import { getConfig } from '../src/config';

interface TestConfig {
  nice: string;
}

process.env.DEPLOYMENT_ENVIRONMENT = process.env.DEPLOYMENT_ENVIRONMENT || 'dev';
const PATH_PREFIX = `/IntSaab2021/${process.env.DEPLOYMENT_ENVIRONMENT}`;

describe('config', () => {
  describe('getConfig', () => {
    test('getConfig with provided return type & schema', async () => {
      const configFeatures = {
        '/path/to/smth/nice': 'nice',
      };

      nock('https://ssm.eu-north-1.amazonaws.com:443', {
        allowUnmocked: true,
        encodedQueryParams: true,
      })
        .post(() => true)
        .reply(200, {
          Parameters: [
            { Name: `${PATH_PREFIX}/path/to/smth/nice`, Value: 'sweet sweet secret' },
          ],
        });

      const { nice } = await getConfig<TestConfig>(
        '/path/to/smth/',
        configFeatures,
      );

      expect(nice).toBe('sweet sweet secret');
    });

    test('getConfig with partial schema', async () => {
      const configFeatures = {
        '/path/to/something/nice': 'nice',
      };

      nock('https://ssm.eu-north-1.amazonaws.com:443', {
        allowUnmocked: true,
        encodedQueryParams: true,
      })
        .post(() => true)
        .reply(200, {
          Parameters: [
            { Name: `${PATH_PREFIX}/path/to/something/nice`, Value: 'sweet sweet secret' },
            { Name: `${PATH_PREFIX}/path/to/something/else`, Value: 'another sweet secret'}
          ],
        });

      
      const { nice, ['/path/to/something/else']: notTyped } = await getConfig<any>(
        '/path/to/something/',
        configFeatures,
      );

      expect(nice).toBe('sweet sweet secret');
      expect(notTyped).toBe('another sweet secret');
    });

    // TODO Is it safe to assume this won't run first?
    test('getConfig should return a cached value', async () => {
      const configFeatures = {
        '/path/to/smth/nice': 'nice',
      };

      const { nice } = await getConfig<TestConfig>(
        '/path/to/smth/',
        configFeatures,
      );

      expect(nice).toBe('sweet sweet secret');
    });
  });
});
