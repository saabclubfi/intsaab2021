import { APIGatewayProxyEventV2, Context } from 'aws-lambda';

import KSUID from 'ksuid';
import { v4 } from 'uuid';

import { ddb } from './db';
import { ApiResponse, Participant, User, Cart } from './interfaces';

import { buildCart } from './cart';

import * as checkout from './checkout';

const errorResponse: ApiResponse = {
  error: 'General error',
};

export const getUser = (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));

  return db
    .getUser()
    .then((user) => (user ? { data: user } : { error: 'User not found' }))
    .catch((e) => {
      console.log('Get user failed', e);
      return errorResponse;
    });
};

export const postUser = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));

  if (!event.body) {
    return { error: 'No request body' };
  }

  // TODO validate body
  let user: User;
  try {
    user = JSON.parse(event.body);
  } catch (e) {
    return { error: 'Malformed body' };
  }

  return db
    .putUser(user)
    .then(() => ({}))
    .catch((e) => {
      console.log('Put user failed', e);
      return errorResponse;
    });
};

export const postParticipant = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));

  if (!event.body) {
    return { error: 'No request body' };
  }

  // TODO validate body
  let user;
  try {
    user = JSON.parse(event.body);
  } catch (e) {
    return { error: 'Malformed body' };
  }

  const id = (await KSUID.random()).string;

  return db
    .putParticipant(id, user, true)
    .then(() => ({ data: { id } }))
    .catch((e) => {
      console.log('Put participant failed', e);
      return errorResponse;
    });
};

export const getParticipants = (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));

  return db
    .getParticipants()
    .then((participants) => ({ data: participants }))
    .catch((e) => {
      console.log('Get participant failed', e);
      return errorResponse;
    });
};

export const putParticipant = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const id = event.pathParameters.id;

  const db = ddb(getAuthorizerSubject(event));

  if (!event.body) {
    return { error: 'No request body' };
  }

  // TODO validate body
  let participant: Participant;
  try {
    participant = JSON.parse(event.body);
  } catch (e) {
    return { error: 'Malformed body' };
  }

  return db
    .putParticipant(id, participant)
    .then(() => ({}))
    .catch((e) => {
      console.log('Put participant failed', e);
      return errorResponse;
    });
};

export const deleteParticipant = (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const id = event.pathParameters.id;

  const db = ddb(getAuthorizerSubject(event));

  return db
    .deleteParticipant(id)
    .then(() => ({}))
    .catch((e) => {
      console.log('Delete participant failed', e);
      return errorResponse;
    });
};

export const getCart = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));
  return buildCart(db)
    .then((cart) => ({
      data: cart,
    }))
    .catch((e) => {
      console.log('Get cart failed', e);
      return errorResponse;
    });
};

export const createPayment = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  const db = ddb(getAuthorizerSubject(event));
  const cart = await buildCart(db);
  try {
    const { email } = await db.getUser();
    const stamp = (await KSUID.random()).string;
    const reference = v4();
    const items: any[] = [cart.user, ...cart.participants];
    const total = cart.totalPrice;
    const language = 'FI';
    const data = await checkout.createPayment(
      stamp,
      reference,
      email,
      language,
      items,
      total,
    );
    await db.putPayment(stamp, {
      reference,
      amount: total,
      'checkout-transaction-id': data.transactionId,
      timestamp: new Date().toISOString(),
    });
    return { data: { ...data, stamp } };
  } catch (e) {
    console.log('Create payment failed', e);
    return errorResponse;
  }
};

export const getPaymentProviders = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<ApiResponse> => {
  return checkout
    .getPaymentProviders()
    .then((data: any) => ({ data }))
    .catch((e) => {
      console.log('Get payment providers failed', e);
      return errorResponse;
    });
};

export const checkoutCallback = async (
  event: APIGatewayProxyEventV2,
  context: Context,
): Promise<any> => {
  try {
    await checkout.validateSignature(event.queryStringParameters);

    // await ddb(getAuthorizerSubject(event)).updatePaymentFromCallback(
    //   checkoutStamp,
    //   checkoutAmount,
    //   checkoutStatus,
    // );

    return { statusCode: 200 };
  } catch {
    return { statusCode: 200 };
  }
};

function getAuthorizerSubject(event: APIGatewayProxyEventV2): string {
  return event?.requestContext?.authorizer?.jwt?.claims?.sub?.toString();
}
