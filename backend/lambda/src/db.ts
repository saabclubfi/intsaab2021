import { DocumentClient } from 'aws-sdk/clients/dynamodb';

import {
  DdbParticipant,
  DdbPayment,
  DdbUser,
  Participant,
  Payment,
  User,
} from './interfaces';

const isTest = Boolean(process.env.JEST_WORKER_ID);

const ddbEndPoint = process.env.DDB_LOCAL || 'http://localhost:8000';
const config: DocumentClient.DocumentClientOptions = {
  convertEmptyValues: true,
  ...(isTest && {
    endpoint: ddbEndPoint,
    sslEnabled: false,
    region: 'local-env',
    maxRetries: 5,
  }),
};

const client = new DocumentClient(config);

async function execute<T>(promise: Promise<any>): Promise<T> {
  try {
    return await promise;
  } catch (e) {
    console.log('DynamoDB request failed', e);
    throw e;
  }
}

export const ddb = (sub: string) => ({
  async getUser(): Promise<User | null> {
    let ddbItem: DocumentClient.GetItemOutput;
    try {
      // TODO Use promisify
      ddbItem = await client
        .get({
          TableName: process.env.DDB_TABLE_NAME,
          Key: {
            PK: `${sub}#USER`,
            SK: 'USER',
          },
        })
        .promise();
    } catch (e) {
      console.log('DynamoDB request failed', e);
      throw e;
    }

    const item = ddbItem.Item;

    return item
      ? {
          name: item.name,
          phone: item.phone,
          address: item.address,
          car: item.car,
          email: item.email,
          subscriptions: item.subscriptions,
        }
      : null;
  },

  async putUser(user: User): Promise<void> {
    let ddbItem: DdbUser = {
      ...user,
      PK: `${sub}#USER`,
      SK: 'USER',
    };

    try {
      await client
        .put({
          TableName: process.env.DDB_TABLE_NAME,
          Item: ddbItem,
        })
        .promise();
    } catch (e) {
      console.log('DynamoDB request failed', e);
      throw e;
    }
  },

  async putParticipant(
    id: string,
    participant: Participant,
    createNew: boolean = false,
  ): Promise<void> {
    let ddbItem: DdbParticipant = {
      ...participant,
      PK: `${sub}#USER`,
      SK: `PARTICIPANT#${id}`,
    };

    const putRequest: any = {
      TableName: process.env.DDB_TABLE_NAME,
      Item: ddbItem,
    };
    if (!createNew) {
      putRequest['ConditionExpression'] =
        'attribute_exists(PK) AND attribute_exists(SK)';
    }
    try {
      const response = await client.put(putRequest).promise();
    } catch (e) {
      console.log('DynamoDB request failed', e);
      throw e;
    }
  },

  async deleteParticipant(id: string) {
    let ddbItem: DocumentClient.DeleteItemOutput;
    try {
      // TODO Use promisify
      ddbItem = await client
        .delete({
          TableName: process.env.DDB_TABLE_NAME,
          Key: {
            PK: `${sub}#USER`,
            SK: `PARTICIPANT#${id}`,
          },
          ConditionExpression: 'attribute_exists(PK) AND attribute_exists(SK)',
        })
        .promise();
    } catch (e) {
      console.log('DynamoDB request failed', e);
      throw e;
    }
  },

  async getParticipants(): Promise<Participant[]> {
    let ddbItems: DocumentClient.QueryOutput;

    const skToParticipantId = (sk: string) => sk.split('#')[1];

    try {
      ddbItems = await client
        .query({
          TableName: process.env.DDB_TABLE_NAME,
          KeyConditionExpression: 'PK = :pk AND begins_with(SK, :sk)',
          ExpressionAttributeValues: {
            ':pk': `${sub}#USER`,
            ':sk': 'PARTICIPANT#',
          },
        })
        .promise();
    } catch (e) {
      console.log('DynamoDB request failed', e);
      throw e;
    }
    return ddbItems.Items.map((item) => ({
      id: skToParticipantId(item.SK),
      name: item.name,
      phone: item.phone,
      address: item.address,
      email: item.email,
      age: item.age,
    }));
  },

  async putPayment(stamp: string, data: any) {
    const ddbItem: DdbPayment = {
      PK: `${sub}#PAYMENT`,
      SK: `PAYMENT#${stamp}`,
      data,
      payment_status: 'new',
    };

    const putRequest: any = {
      TableName: process.env.DDB_TABLE_NAME,
      Item: ddbItem,
    };

    await execute<DocumentClient.PutItemOutput>(
      client.put(putRequest).promise(),
    );
  },

  async getPayment(stamp: string): Promise<Payment | null> {
    const { Item: item } = await execute<DocumentClient.GetItemOutput>(
      client
        .get({
          TableName: process.env.DDB_TABLE_NAME,
          Key: {
            PK: `${sub}#PAYMENT`,
            SK: `PAYMENT#${stamp}`,
          },
        })
        .promise(),
    );

    return item
      ? {
          data: item.data,
          payment_status: item.payment_status,
        }
      : null;
  },

  // NOT WORKING YET :(
  async updatePaymentFromCallback(
    stamp: string,
    amount: number,
    status: string,
  ): Promise<void> {
    const { Items: [Item] } = await execute<DocumentClient.ScanOutput>(
      client
        .scan({
          TableName: process.env.DDB_TABLE_NAME,
          FilterExpression: 'SK = :sk',
          ExpressionAttributeValues: {
            // ':pk': `#PAYMENT`,
            ':sk': {
              S: `PAYMENT#${stamp}`,
            },
          },
        })
        .promise(),
    );

    await execute<DocumentClient.PutItemOutput>(
      client
        .put({
          TableName: process.env.DDB_TABLE_NAME,
          Item: {
            ...Item,
            payment_status: status,
            payment_paid: amount,
          },
          ConditionExpression: 'SK = :sk',
          ExpressionAttributeValues: {
            ':sk': `PAYMENT#${stamp}`,
          },
        })
        .promise(),
    );
  },
});
