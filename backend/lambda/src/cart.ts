
import { Cart, Participant } from './interfaces';
import { getConfig } from './config';

// TODO try to get 'db' a proper type
export const buildCart = async (db: any): Promise<Cart> => {
  const priceConfig = await getConfig<{ [key: string]: string }>('/Price');

  const [user, participants] = await Promise.all([
    db.getUser(),
    db.getParticipants(),
  ]);

  const cartBuilders = [
    (cart: Cart): Cart =>
      user
        ? {
            ...cart,
            user: {
              name: user.name,
              unitPrice: parseInt(priceConfig['/Price/User'], 10),
            },
          }
        : { ...cart },
    (cart: Cart): Cart =>
      participants
        ? {
            ...cart,
            participants: participants.map((p: any) => ({
              name: p.name,
              age: parseInt(p.age, 10),
              unitPrice: getUnitPrice(p, priceConfig),
            })),
          }
        : { ...cart },
    (cart: Cart): Cart => ({
      ...cart,
      totalPrice: calculateTotal(cart),
    }),
  ];

  return cartBuilders.reduce((prev, curr) => curr(prev), {
    totalPrice: 0,
    participants: [],
  });
};

function getUnitPrice(
  participant: Participant,
  priceConfig: { [key: string]: string },
): number {
  const participantAge = parseInt(participant.age, 10);
  const defaultPrice = parseInt(priceConfig['/Price/Participant'], 10);
  const agePrices: [number, number][] = Object.keys(priceConfig)
    .filter((key) => key.startsWith('/Price/Participant/'))
    .map((key) => parseInt(key.split('/').slice(-1)[0], 10))
    .sort((a, b) => b - a)
    .map((key) => [
      key,
      parseInt(priceConfig[`/Price/Participant/${key}`], 10),
    ]);
  return agePrices.reduce(
    (prev, [age, price]) => (participantAge < age ? price : prev),
    defaultPrice,
  );
}

function calculateTotal(cart: Cart): number {
  return (
    (cart?.user?.unitPrice || 0) +
    cart.participants.reduce((prev, curr) => prev + curr.unitPrice, 0)
  );
}
