import * as crypto from 'crypto';
import got from 'got';
import { v4 } from 'uuid';

import { getConfig } from '../config';

interface PaymentConfig {
  CHECKOUT_ACCOUNT: string;
  CHECKOUT_SECRET_KEY: string;
}

const gotRequestTimeout = process.env.API_REQUEST_TIMEOUT || '5000';

const configFeatures = {
  '/IntSaab2021/Dev/Checkout/Account': 'CHECKOUT_ACCOUNT',
  '/IntSaab2021/Dev/Checkout/SecretKey': 'CHECKOUT_SECRET_KEY',
};

const API_URL = 'https://api.checkout.fi';
const FRONTEND_URL = process.env.FRONTEND_URL;
const BACKEND_URL = process.env.BACKEND_URL;

export const toItem = (item: any) => {
  const now = new Date().toISOString();

  return {
    unitPrice: item.unitPrice,
    units: 1,
    vatPercentage: 0,
    productCode: 'code',
    deliveryDate: now.split('T')[0],
  };
};

const calculateHmac = (secret: string, params: any, body: any = undefined) => {
  const hmacPayload = Object.keys(params)
    .sort()
    .map((key) => [key, params[key]].join(':'))
    .concat(body ? JSON.stringify(body) : '')
    .join('\n');

  return crypto.createHmac('sha256', secret).update(hmacPayload).digest('hex');
};

const buildHeaders = (
  method: string,
  account: string,
  transactionId: string = null,
) => {
  const headers: any = {
    'checkout-account': account,
    'checkout-algorithm': 'sha256',
    'checkout-method': method,
    'checkout-nonce': v4(),
    'checkout-timestamp': new Date().toISOString(),
  };

  if (transactionId) {
    headers['checkout-transaction-id'] = transactionId;
  }

  return headers;
};

export const createPayment = async (
  stamp: string,
  reference: string,
  email: string,
  language: string,
  items: any[],
  total: number,
): Promise<any> => {
  const {
    CHECKOUT_ACCOUNT = '375917',
    CHECKOUT_SECRET_KEY = 'SAIPPUAKAUPPIAS',
  } = await getConfig<PaymentConfig>('/Checkout/', configFeatures);
  const body = {
    stamp,
    reference,
    amount: total,
    currency: 'EUR',
    language,
    items: items.map(toItem),
    customer: {
      email,
    },
    callbackUrls: {
      success: `${BACKEND_URL}/callback/checkoutFi`,
      cancel: `${BACKEND_URL}/callback/checkoutFi`,
    },
    redirectUrls: {
      success: `${FRONTEND_URL}/payment/success`,
      cancel: `${FRONTEND_URL}/payment/cancel`,
    },
  };

  const headers = buildHeaders('POST', CHECKOUT_ACCOUNT);
  const data = await got.post(`${API_URL}/payments`, {
    headers: {
      ...headers,
      signature: calculateHmac(CHECKOUT_SECRET_KEY, headers, body),
    },
    json: body,
    timeout: parseInt(gotRequestTimeout, 10),
    responseType: 'json',
    resolveBodyOnly: true,
  });

  return data;
};

export const getPaymentProviders = async () => {
  const {
    CHECKOUT_ACCOUNT = '375917',
    CHECKOUT_SECRET_KEY = 'SAIPPUAKAUPPIAS'
  } = await getConfig<PaymentConfig>(
    '/Checkout/',
    configFeatures,
  );
  const headers = buildHeaders('GET', CHECKOUT_ACCOUNT);
  const data = await got.get(`${API_URL}/merchants/payment-providers`, {
    headers: {
      ...headers,
      signature: calculateHmac(CHECKOUT_SECRET_KEY, headers),
    },
    timeout: parseInt(gotRequestTimeout, 10),
    responseType: 'json',
    resolveBodyOnly: true,
  });
  return data;
};

export const validateSignature = async (query: any) => {
  const { signature, ...checkoutParameters } = query;
  const { CHECKOUT_SECRET_KEY = 'SAIPPUAKAUPPIAS' } = await getConfig<PaymentConfig>(
    '/Checkout/',
    configFeatures,
  );
  const isSignatureValid =
    calculateHmac(CHECKOUT_SECRET_KEY, checkoutParameters, '') === signature;

  if (isSignatureValid && checkoutParameters['checkout-status'] === 'ok') {
    return Promise.resolve({
      checkoutAmount: checkoutParameters['checkout-amount'],
      checkoutStamp: checkoutParameters['checkout-stamp'],
      checkoutStatus: checkoutParameters['checkout-status'],
    });
  }

  return Promise.reject('Signature validation failed');
};
