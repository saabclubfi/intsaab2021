interface DataObject {
  [key: string]: any;
}

export interface ApiResponse {
  data?: DataObject;
  error?: string;
}

interface DdbItem {
  PK: string;
  SK: string;
}

export interface User {
  name: string;
  phone?: string;
  address?: object;
  car?: string;
  email: string;
  subscriptions?: [string];
}

export interface DdbUser extends DdbItem, User {}

export interface Participant {
  id?: string;
  name: string;
  phone?: string;
  address?: object;
  email?: string;
  age: string;
}

interface CartItem {
  name: string;
  age?: number;
  unitPrice: number;
}

export interface Cart {
  totalPrice: number;
  payments?: [];
  user?: CartItem;
  participants: CartItem[];
  pretours?: [];
  products?: [];
}

export interface DdbParticipant extends DdbItem, Participant {}

export interface Payment {
  data?: object;
  payment_status?: string;
}

export interface DdbPayment extends DdbItem, Payment {}
