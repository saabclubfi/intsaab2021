import AWS from 'aws-sdk';

const isTest = Boolean(process.env.JEST_WORKER_ID);
const ssmClientConfiguration: AWS.SSM.ClientConfiguration = {
  ...(isTest && {
    region: 'eu-north-1',
  }),
};
const ssm = new AWS.SSM(ssmClientConfiguration);

const cache: any = {};

export async function getConfig<T>(
  path: string,
  schema: { [key: string]: string } = null,
): Promise<T> {
  if (cache[path]) {
    return {
      ...cache[path],
    } as T;
  }

  const pathPrefix = `/IntSaab2021/${process.env.DEPLOYMENT_ENVIRONMENT}`;

  // TODO Check NextToken and retrieve more if set
  const { Parameters }: AWS.SSM.GetParametersByPathResult = await ssm
    .getParametersByPath({
      Path: `${pathPrefix}${path}`,
      Recursive: true,
      WithDecryption: true,
    })
    .promise();

  const config = Parameters.map(({ Name, Value }) => ({
    Name: Name.slice(pathPrefix.length),
    Value,
  })).reduce(
    (config: T, { Name, Value }) => ({
      ...config,
      ...(schema && schema[Name]
        ? { [schema[Name]]: Value }
        : { [Name]: Value }),
    }),
    {} as T,
  );

  cache[path] = config;

  return {
    ...cache[path],
  } as T;
}

export function clearCache() {
  for (const key of Object.keys(cache)) {
    delete cache[key];
  }
}
